<?php namespace TMSApp\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class BackupDataEndOfDay extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'tms:backup-data';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$dbhost   = env('DB_HOST', 'localhost');
		$dbuser   = env('DB_USERNAME', 'forge');
		$dbpwd    = env('DB_PASSWORD', '');
		$dbname   = env('DB_DATABASE', 'forge');
		$dumpfile = storage_path().'/backup/'. date("Y-m-d_H-i-s") . "_" . $dbname . ".gz";

		$cmd = "/usr/bin/mysqldump --opt --host=$dbhost --user=$dbuser --password=$dbpwd $dbname | gzip > $dumpfile";
		system($cmd);

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// ['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			// ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
