<?php namespace TMSApp\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Carbon\Carbon;

use TMSApp\Repositories\ReportRepository;
use TMSApp\Repositories\MissingReportRepository;
use TMSApp\Repositories\ReportCategoryRepository;
use TMSApp\Repositories\UserRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Mail;
use TMSApp\Repositories\SettingRepository;
use Input;
class UpdateCatalog extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'amazon:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

    private $now;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
        $this->now = Carbon::now();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire(MissingReportRepository $missing_report, ReportRepository $report, UserRepository $user, SettingRepository $model)
	{
		// $now = Carbon::now();
        // $day = $now->day;
        $day = $this->now->day;
        // $month = $now->month;
        $month = $this->now->month;
        // $year = $now->year;
        $year = $this->now->year;
        if( $day == 1 ){
            $this->createNewRowMissing($user, $missing_report);
       }
        $monthChedule = $missing_report->missingReport($month, $year);
        $this->installMissingReport($monthChedule, $report, $missing_report, $model);

        // check day < 5. Install last month
        if($day <= 5){
            $monthChedule = $missing_report->missingReport($month-1, $year);
            $this->installMissingReport($monthChedule, $report, $missing_report, $model, $last_month = 1);
        }

        //
        $listDayForgot = $this->missingReport($missing_report, $user);

        foreach ($listDayForgot as $value) {
            $infoUser = $user->find($value['user_id']);
            if(isset($value['day_forget']) && $infoUser->id != 1)
            {
                $email = $infoUser->email;
                $name = $infoUser->name;
                $listDay = implode(',', $value['day_forget']);
                if(app()->environment() == 'production'){
                    $this->SendEmailUserForgot($email, $listDay, $name);
                } //end if
            }
        }
	}

    // install table missingReport
    private function installMissingReport($monthChedule, $report, $missing_report, $model, $last_month = null)
    {
        foreach ($monthChedule as $key => $value) {
            $pattern = array('id'=>$value->id);
            // Get status for day.
            if($last_month != null) {
                $result = $this->checkMissingReport($value, $report, $model, $last_month);
            } else {
                $result = $this->checkMissingReport($value, $report, $model);
            }
            // Update missing.
            $missing_report->update($result , $pattern);
        }
    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// ['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			// ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

    /**
     * Create new row missing_report
     * @param  UserRepository          $user
     * @param  MissingReportRepository $missingReport
     * @return boolean
     */
    public function createNewRowMissing($user, $missingReport)
    {
        // Get all member
        $result = $user->all();
        $member = $result->count('id');
        // Get this date
        // $now = Carbon::now();
        $month = $this->now->month;
        $year = $this->now->year;
        //Loop member
        for($i=0 ; $i<$member; $i++){
            try{
                $user_id = $result[$i]->id;
                $data = ['user_id'=> $result[$i]->id, 'month'=>$month, 'year'=>$year];
                // Check duplication
                $_result = $missingReport->getByPatter($data);
                if($_result->isEmpty()){
                    // Insert new row into table missing_report
                    $missingReport->create($data);
                }else{
                    return 0;
                }
            }catch (\Exception $e) {
                //Write log if error.
                Log::error($e);
            }
        }
        return 1;
    }

	/**
     * Update status for day
     * @param int $cost
     * @param string $thisDayColumn
     * @param collection $result
     * @param model injection $missing_report
     * @return boolean
     */
    private function setStatusForWorkingDay($cost, $thisDayColumn, $result)
    {
        if ($cost < 8 ){
            $status = TMS_MISSING;
        }else{
            $status = TMS_REPORT_DONE;
        }
        return $status;
    }

    /**
     * Calculation Cost.
     * @return int
     */
    private function costOfUserInDay($thisDay, $user_id, $report,$data_year, $data_month)
    {
        $time = [
            'start'=>" 08:00:00",
            'end'  =>" 18:00:00"
        ];
        $time_missing = Carbon::create($data_year, $data_month);
        $result = $report->reportInDay($user_id, $thisDay, $time_missing,$time);
        $cost = 0;
        $cost_work_off = 0;
        if($result){
            foreach ($result as $key => $value) {
                $cost = $cost + $value->cost;
                if( $value->type == TMS_WORKOFF_TYPE ){
                    $cost_work_off = $cost_work_off + $value->cost;
                }
            }
        }

        if ( $cost_work_off/60 >= 8 ){
            return TMS_DAYOFF;
        }
        if ( $cost/60 < 8 ){
            return TMS_MISSING;
        }else{
            return TMS_REPORT_DONE;
        }
    }

    private function getdayoff($model) {
        $data = $model->getlist();
        if(count($data) > 0)  {
            return $dayoff = unserialize($data->setting_value);
        }
        return array();
    }

    /**
     * Check missing report.
     * @return boolean
     */
    private function checkMissingReport($data, $report, $model, $last_month = null)
    {
        if($last_month != null ) {
            $last_month = $this->now->month - 1;
            $t_month = Carbon::create($data->year, $last_month);
            $dayOfMonthNow = $t_month->daysInMonth;
            $data->month = $last_month;
        } else {
            $dayOfMonthNow = $this->now->day;
        }

        $result = [];
        $holiday = $this->getdayoff($model);
        for ($i=1; $i <= $dayOfMonthNow; $i++) {
            $isWeekend = Carbon::createFromDate($data->year, $data->month, $i)->isWeekend();
            $thisDayColumn = 'day_'.$i;

            $dayBetweenMonth = Carbon::create($data->year, $data->month, $i, 0, 0, 0);
            $key = array_search($dayBetweenMonth, array_column($holiday, 'day'));


            if( !$isWeekend ){

                if($data[$thisDayColumn] == TMS_DAYOFF){

                    $result[$thisDayColumn] = TMS_DAYOFF;

                }else{
                    // Calculation cost.
                    $result[$thisDayColumn] = $this->costOfUserInDay($i, $data->user_id, $report,$data->year, $data->month);
               }
                if(isset($holiday[$key]['day'])) {
                    if($holiday[$key]['day'] == $dayBetweenMonth) {
                     $result[$thisDayColumn] = TMS_DAYOFF;
                    }
                }
            }else{
                $result[$thisDayColumn] = TMS_WEEKEND;
            }
        }
        return $result;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    private function missingReport( $report, $UserModel)
    {
        $data = Input::all();
        $listUserDelete = $UserModel->getUserOlyTrash();

        $month  = isset($data['month']) ?  $data['month'] : 0;
        $year   = isset($data['year']) ?  $data['year'] : 0;
        $result = $report->missingReport($month, $year,null, $listUserDelete);
        $data = [];
        $now = Carbon::now();
        if(!$result->isEmpty()){
            if($month == 0 || $month == $now->month){
                $toDay = $now->day;
            }else {
                $now = Carbon::createFromDate($result->first()->year, $result->first()->month,1);
                $toDay = $now->daysInMonth;
            }

            foreach ($result as $key => $value) {
                $data[] = $this->missingReportTemplate($value, $toDay);
            }
        }
        return $data;
    }

    /**
     * Fill data with template
     * @param array $result
     * @param  array $categories
     * @return response
     */
    private function missingReportTemplate($result, $loop)
    {
        $data = [];
        $data['user_id'] = $result['user_id'];
        for ($i=1; $i <= $loop; $i++) {
            $isWeekend = Carbon::createFromDate($result->year, $result->month, $i)->isWeekend();
            $thisDayColumn = 'day_'.$i;
            if( !$isWeekend ){
                if($result[$thisDayColumn] == TMS_MISSING){
                    $data['day_forget'][] = $i;
                }
            }
        }
        return $data;
    }

    /**
     * Send email for user forgot report
     */
    private function SendEmailUserForgot($email,$listday, $name)
    {

        Mail::send('emailMissingreport', [ 'email' => $email, 'listday' => $listday, 'name'=> $name], function ($message) use ($email) {
            $message->from(env('MAIL_USERNAME'), '[TMS] Missing report');
            $message->to($email)->subject('[TMS] Missing report ');
        });

    }
}
