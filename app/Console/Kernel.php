<?php namespace TMSApp\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'\TMSApp\Console\Commands\Inspire',
	    '\TMSApp\Console\Commands\StartApp',
	    '\TMSApp\Console\Commands\UpdateCatalog',
	    '\TMSApp\Console\Commands\BackupDataEndOfDay',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('amazon:update')->dailyAt('23:00');
		$schedule->command('tms:backup-data')->dailyAt('00:00');
	}

}
