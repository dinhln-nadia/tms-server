<?php namespace TMSApp\Http\Controllers;

use Validator;
use Input;
use Response;
use HttpResponse;
use TMSApp\Http\Controllers\Controller;
use TMSApp\Repositories\ReportCategoryRepository;
use TMSApp\Repositories\ReportRepository;
use TMSApp\Repositories\CategoryRepository as Category;


class CategoryController extends Controller {

    private $model;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Category $model)
    {
        $this->middleware('jwt.auth');
        $this->middleware('jwt.refresh');
        $this->middleware('admin', ['except'=>['index']]);
        $this->model = $model;
    }

    /**
     * Switch type of category
     * @param string $input
     * @return int
     */
    public function getType($input = ''){

        switch ($input) {
            case 'category':
                $result = TMS_CATEGORY;
                break;
             case 'process':
                $result = TMS_PROCESS;
                break;
            default:
                $result = 0;
        }
        return $result;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Input::all();

        // Create validator
        $validator = Validator::make($data, [
            'type' => 'required|alpha|max:255',
        ]);
        
        // Thorw message error if fail.
        if ($validator->fails()) {
            return Response::json(['error' => $validator->messages()], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        
        // Switch type of category
        $type = $this->getType( Input::get('type') );
        
        // Thorw message error if fail.
        if ( $type == 0 ){
            return Response::json(['error' => trans('message.category_type')], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        
        $data = $this->model->getCategories($type);
        $status= TMS_STATUS;
        return Response::json(compact('status', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data = Input::all();
        // Create validator
        $validator = Validator::make($data, [
            'name' => 'required|max:255',
        ]);
        
        // Thorw message error if fail.
        if ($validator->fails()) {
            return Response::json(['error' => $validator->messages()], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        
        // Switch type of category
        $type = $this->getType( Input::get('type') );
        // Thorw message error if fail.
        if ( $type == 0 ){
            return Response::json(['error' => trans('message.category_type')], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        try {
            $data['type'] = $type;
            $data = $this->model->create($data);

        } catch (\Exception $e) {   
            return Response::json(['error' => trans('message.category_exists')], HttpResponse::HTTP_CONFLICT);
        }
        $status= TMS_STATUS;
        return Response::json(compact('status', 'data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Category $model)
    {
        $data = Input::all();
        $model->update($data, $id, 'id');
        
        $status= TMS_STATUS;
        return Response::json(compact('status'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, ReportCategoryRepository $categoryReportModel, Category $cate)
    {
            $develop = 0;
            $type = $this->model->find($id);
            $result = $this->model->delete($id);

            if( $type['type'] == 1 ) {
                $develop = DEVELOPMENT_CATEGORY_ID;
            } else {
                $develop = DEVELOPMENT_PROCESS_ID;
            }

            $category_id = $cate->getOnlyWithTrashed($type['type'])->toArray();
            $categoryReportModel->UpdateCategoryIdHasDelete($id,$category_id, $develop);

        if($result == 1){
            return Response::json(array('status'=>'success'));
        }

        return Response::json(['error' => trans('message.update_error')], HttpResponse::HTTP_NOT_ACCEPTABLE);
    }

}
