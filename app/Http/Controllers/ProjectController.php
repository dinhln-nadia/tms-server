<?php namespace TMSApp\Http\Controllers;

use TMSApp\Http\Requests;
use TMSApp\Http\Controllers\Controller;
use Response;
use Illuminate\Http\Request;
use TMSApp\Repositories\ProjectRepository as Project;
use TMSApp\Repositories\ProjectUserRepository;
use TMSApp\Repositories\UserRepository;
use TMSApp\Repositories\ReportRepository;
use TMSApp\Repositories\ReportCategoryRepository;
use TMSApp\Repositories\CategoryRepository;
use Carbon\Carbon;
use JWTAuth;
use HttpResponse;
use Validator;
use Exception;
use Illuminate\Support\Collection;
use Input;
use TMSApp\Models\Projects as Test;

class ProjectController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
        $this->middleware('jwt.refresh');

        $this->middleware('admin', ['except'=>['index','myDashboard','getAllNameOfProject']]);
        // $this->middleware('admin');
    }

    /**
     * Add timestamps, check duplicate in database.
     * @param array $data
     * @return array
     */
    private function fillData($data, $model)
    {

        // Convert string users_id to array.
        $integerIDs = array_map('intval', explode(',', $data['users_id']));

        // Get data user to check duplicate.
        $fromDB = $model->getUserId($data['project_id'],$integerIDs);

        $arrDB      = $fromDB->toArray();
        $projectId  = intval($data['project_id']);
        $j      = count($integerIDs);
        $now    = Carbon::now();
        $result = [];

        for ($i=0; $i < $j; $i++) {
            // Create array user to check duplicate.
           $check = array( 'user_id'    => $integerIDs[$i],
                           'project_id' => $projectId,
                           'member_quotation' => 0
                          );
            // Check duplicate in database.
            if( !in_array( $check, $arrDB) ){
                $result[$i]['user_id']    = $integerIDs[$i];
                $result[$i]['project_id'] = $projectId;
                $result[$i]['created_at'] = $now;
                $result[$i]['updated_at'] = $now;
            }
        }
        return $result;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Project $model)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $result = $model->getDeltail($user);
        $data = $result->all();
        $status = TMS_STATUS;
        return Response::json(compact('status', 'data') );
    }

    /**
     * Display all projects in database.
     *
     * @return Response
     */
    public function getAllNameOfProject(Project $model)
    {
        $result = $model->getAllName();
        $data = $result->all();
        $status = TMS_STATUS;
        return Response::json(compact('status', 'data') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Project $model)
    {
        $data = Input::all();
        $user = JWTAuth::parseToken()->authenticate();
        $validator = Validator::make($data, [
            'name'   => 'required|max:255',
            'quotation_time'=>'max:6',
            'plan_time' => 'max:6'
        ]);

        // If validation fails return error message.
        if ($validator->fails()) {

            return Response::json([
                'error' => $validator->messages()
            ], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        //If create new project fails return error
        try {
            if($user->is('admin')){
                $data = $model->create($data);
            }
        } catch (\Exception $e) {
            return Response::json(['error' => trans('message.project_exists')], HttpResponse::HTTP_CONFLICT);
        }

        $status = TMS_STATUS;
        return Response::json(compact('status', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function addUser($id, ProjectUserRepository $model)
    {
        $data = [];
        $data['users_id'] = Input::get('users_id');
        $data['project_id'] = $id;
        $user = JWTAuth::parseToken()->authenticate();
        // Create validator
        $validator = Validator::make($data, [
            'users_id'      => 'required|string',
            'project_id'    => 'required|string',
        ]);

        // If validation fails return error message.
        if ($validator->fails()) {
            return Response::json([
                'error' => $validator->messages()
            ], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        $data = $this->fillData($data, $model);

        try {
            if($user->is('admin')){
                $data = $model->insertProject($data);
            }
        } catch (\Exception $e) {
            return Response::json(['error' => trans('message.project_user_exists')], HttpResponse::HTTP_CONFLICT);
        }
        $status = TMS_STATUS;
        return Response::json(compact('status'));
    }

    /**
     * Get all user not in project.
     *
     * @param  string  $project_id
     * @return Response
     */
    public function getUserNotInProject($project_id, UserRepository $users, ProjectUserRepository $model)
    {
        $list_user_id = $model->getUserInproject($project_id);
        $list_user_in_project = [];
        foreach ($list_user_id as $value) {
            $list_user_in_project[] = $value->user_id;
        }
        $result = $users->getListUser($list_user_in_project);
        $data = $result->all();
        $status = TMS_STATUS;
        return Response::json(compact('status','data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id, Project $model)
    {
         $result = $model->getProjectDetail($id);
         $data = $result->all();
         $data[0]['count'] = $result[0]->users->count();

         $status = TMS_STATUS;
         return Response::json(compact('status','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Project $model)
    {
        $data = Input::all();
        $validator = Validator::make($data, [
            'name'   => 'required|max:255',
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        // If validation fails return error message.
        if ($validator->fails()) {

            return Response::json([
                'error' => $validator->messages()
            ], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        if($user->is('admin')){
            $result = $model->update($data, $id, $attribute='id');
            if($result == 1){
                return Response::json(array('status'=>'success'));
            }
        }
        return Response::json(['error' => trans('message.update_error')], HttpResponse::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Project $model)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if($user->is('admin')){
            $result = $model->delete($id);
            if($result == 1){
                return Response::json(array('status'=>'success'));
            }
        }
        return Response::json(['error' => trans('message.update_error')], HttpResponse::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function removeUser($id, ProjectUserRepository $model)
    {
        $data = Input::all();


        // Create validator
        $validator = Validator::make($data, ['users_id'  => 'required|string'] );

        // If validation fails return error message.
        if ($validator->fails()) {
            return Response::json([
                'error' => $validator->messages()
            ], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        // Convert string users_id to array.
        $integerIDs = array_map('intval', explode(',', $data['users_id']));

        $user = JWTAuth::parseToken()->authenticate();
        if($user->is('admin')){

            $result = $model->removeUser( $id, $integerIDs );
        }
        if($result >= 1){
            return Response::json(array('status'=>'success'));
        }
        return Response::json(['error' => trans('message.update_error')], HttpResponse::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * Search the specified resource from storage.
     *
     * @param  string  $name
     * @return Response
     */
    public function search(Project $model)
    {
        $data = Input::except('token');

        $validator = Validator::make($data, [
            'name'   => 'required|max:255',
            'option' => 'required|boolean'
        ]);

        // If validation fails return error message.
        if ($validator->fails()) {

            return Response::json([
                'error' => $validator->messages()
            ], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        $pattern           = $data['name'].'%';
        $result = $model->search($pattern, $data['option']);

        $data = $result->all();
        $status = TMS_STATUS;

        return Response::json(compact('status', 'data'));
    }

    /**
     * Get a listing disable of the resource.
     *
     * @return Response
     */
    public function listDisable(Project $model)
    {
        $result = $model->listDisable(['id', 'name']);
        $data = $result->all();
        $status = TMS_STATUS;

        return Response::json(compact('status', 'data'));
    }

     /**
     * Get a listing disable of the resource.
     *
     * @return Response
     */
    public function enableId($id, Project $model)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if($user->is('admin')){
            $result = $model->enable([$id]);
        }
        if($result >= 1){
            return Response::json(array('status'=>'success'));
        }
        return Response::json(['error' => trans('message.update_error')], HttpResponse::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * @param array, id project, id user
     */
    public function ProjectUserRepository(Project $model,ProjectUserRepository $modelUserProject)
    {
        $data = Input::all();
        $project_id = $data['project_id'];
        $user_id = $data['user_id'];
        $timeQuotation = $data['quotation'];
        if($timeQuotation == null || !is_numeric($timeQuotation)) {
            return Response::json(['status' => 'Please enter a number.'], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        $time = $model->checkQuotationTime($project_id);
        if($time == 0)
        {
            return Response::json(['status' => 'The quotation project is 0.'], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        try{
            $timeFree = $this->checkFreeQuotationTime($time, $project_id, $modelUserProject,$user_id);
        }
        catch(\Exception $e){

            return Response::json(['status' => $e->getMessage()]);

        }

        if($timeFree <= 0) {

            return Response::json(['status' => 'Time out!'], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        if($timeQuotation > $timeFree ){

            return Response::json(['status' => 'Quotation time must be greater than free time!'], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        $modelUserProject->updateQuotation($timeQuotation, $project_id, $user_id);

        return Response::json(array('status'=>'success.'));

    }

    public function getFreeTime(Project $model, $project_id, ProjectUserRepository $modelUserProject)
    {
        $time = $model->checkQuotationTime($project_id);
        $timeUser = $modelUserProject->checkQuotationTimeUseForFreeTime($project_id);
        $data = $time - $timeUser;

        return Response::json(array('status'=>'success', 'data'=>$data));
    }

    /**
     * @param number
     * @return number
     */
    public function checkFreeQuotationTime($time, $project_id, $model, $user_id)
    {

        try{

            $timeTotal = $model->checkQuotationTimeUse($project_id, $user_id);

        }
        catch(\Exception $e)
        {
            throw new Exception($e->getMessage());
        }
        return $time - $timeTotal;
    }

    /**
     * Dashboard
     */

    public function myDashboard(
        ProjectUserRepository $modelProjectUser,
        ReportRepository $modelReport,
        ReportCategoryRepository $modelReportCategory,
        CategoryRepository $modelCategory
    )
    {
        $dataInput = Input::all();
        $timeNow = $this->getTimeForDashboard($dataInput);
        $data2 = array(

                "categoryTicket"        =>      ["function_development","other"],
                "nameCategory"          =>      ["Function development","Other"],
                "key"                   =>      ['project_quotation', 'actual_work'],
                "name"                  =>      ['Quotation', 'Actual Work']
            );

        $data = [];
        $user_id  = JWTAuth::parseToken()->authenticate()->id;

        // get list project of user
        $listProject = $modelProjectUser->getListProject($user_id);

        // List category.
        $listCategories = $modelCategory->getData('type',TMS_CATEGORY,['id'])->toArray();

        // List category_report
        $listReportCategory   =  $modelReportCategory->getListReportCategory($listCategories, $user_id, $timeNow['start_time'], $timeNow['end_time']);

        foreach ($listProject as $key => $value) {

            // Get other end development time
            $otherDevelopment = $this->getFunctionDevelopment($value->project_id, $listReportCategory);

            $data[] = [
                "label"                 => $value->label,
                "value"                 => $otherDevelopment['actualWork']/60,
                "project_quotation"     => $value->project_quotation,
                "actual_work"           => $otherDevelopment['actualWork']/60,
                "function_development"  => $otherDevelopment['development']/60,
                "other"                 => $otherDevelopment['other']/60
            ];

         }// end foreach

        return Response::json(array('status'=>'success', 'data'=>array($data, $data2)));
    }

    private function getTimeForDashboard($data)
    {
        $now  = Carbon::now();
        $startTime = $now->year.'-'.$now->month.'-01 '.'00:00:00';
        $endTime   = $now->year.'-'.$now->month.'-'.$now->day.' 23:59:00';


        if(isset($data['start_time']))
            $startTime = $data['start_time'];
        if(isset($data['end_time']))
            $endTime = $data['end_time'];

        return array('start_time' => $startTime, 'end_time' => $endTime);

    }
    /**
     *@param number : Project id, opject list report
     *@return array cost function development
     */
    private function getFunctionDevelopment($project_id, $listReportCategory)
    {
        $other = 0;
        $development = 0;
        $actualWork = 0;

        foreach ($listReportCategory as $key => $value) {
            if( $value->project_id == $project_id) {

                $actualWork = $actualWork + $value->cost;

                if($value->ticket == 0) {

                    $other = $other + $value->cost;
                }
                else {

                    $development = $development + $value->cost;
                }
            }

        }//end foreach

        return array('other'=> $other, 'development'=> $development, 'actualWork' => $actualWork);
    }

    /**
     * Show data for company dashboard
     */
    public function companyDashboard (Project $project, ReportRepository $report)
    {
        $data = [];
        $data2= [
            "key" => [ 'actual_work', 'project_quotation' ],
            "name"=> [ 'Actual Work', 'Project quotation' ]
          ];
        $dataInput = Input::all();
        $timeNow = $this->getTimeForDashboard($dataInput);
        $reports = $report->getReportBetweenTime($timeNow['start_time'], $timeNow['end_time']);
        // get list project of user

        $listProject = $project->getAllProjecct();

        foreach ($listProject as $key => $value) {

            $data[] = [
                "project_id"            => $value->project_id,
                "label"                 => $value->label . ' ~ ' .round(($this->getActualWork($reports, $value->project_id))/8) . 'md',
                "value"                 => $this->getActualWork($reports, $value->project_id),
                "project_quotation"     => $value->project_quotation,
                "actual_work"           => $this->getActualWork($reports, $value->project_id)
            ];

         }// end foreach
        $data = $this->convertArraySort($data, 'actual_work');
        return Response::json(array('status'=>'success', 'data'=>array($data, $data2)));
    }

    /**
     *@param array
     *@return number
     */
    private function getActualWork($reports, $project_id)
    {

        return $data = $reports->where('project_id', $project_id)->sum('cost')/60;

    }

        /**
     * Template of report
     * @param  array $value
     * @return response
     */
    private function templateReport($value)
    {
        $data['id']           = $value->id;
        $data['project_id']   = $value->project_id;
        $data['user_id']      = $value->user_id;
        $data['note']         = $value->note;
        $data['ticket']       = $value->ticket;
        $data['cost']         = $value->cost;
        $data['start_time']   = $value->start_time;
        $data['end_time']     = $value->end_time;
        $data['process_id']      = $value->process->first()->id;
        $data['category_id']     = $value->category->first()->id;
        $data['type']            = $value->type;

        return $data;
    }
    /**
     * Fill data with template
     * @param  collection $result
     * @param  array $categories
     * @return response
     */
    private function fillReportData($result, $categories = true)
    {
        $data = [];
        foreach($result as $key=>$value){
           $data[] = $this->templateReport($value);
        }
       return $data;
    }

    public function projectDashboard (
        Project $projects,
        ProjectUserRepository $modelProjectUser,
        CategoryRepository $modelCategory,
        ReportRepository $report,
        UserRepository $userResposi
    )
    {
        $input = input::all();

        $timeNow = $this->getTimeForDashboard($input);

        $project_id = $input['project_id'];
        $user_id  = JWTAuth::parseToken()->authenticate()->id;

        $data1[] = [
              "costUserKey"           => ['project_quotation','actual_work'],
              "costUserUabel"         => ['Project quotation','Actual work'],
              "categoryKeyTicket"     => ['ticket'],
              "workTime"              => ['work_time'],
              "workTimeLabel"         => ['work time']
              ];

        // Get project by id.
        $project = $projects->findBy('id', $project_id);
        // Get list user has deleted
        $listDelete = $userResposi->getUserOlyTrash();
        // Get all user in project.
        $userProject = $modelProjectUser->getUserProject($project_id, $listDelete);
        // Get all category of system.
        $listCategorieAll = $modelCategory->getwithTrashed();
        // Get report.
        $reports = $report->getReports($project_id, $timeNow);
        // Get report where category.
        $reportsOfCategory = $report->reportsOfCategory($project_id, $timeNow);

        $dataCategory = [];
        $dataProcess  = [];
        foreach ($listCategorieAll as $key => $value) {
            if($value['name'] <> "-"){
                if($value['type'] == TMS_CATEGORY) {
                    $dataCategory [] = [
                        "label" => $value['name']. ' ~ ' .round( ( $reportsOfCategory->where('category_id', $value->id)->sum('cost') /60 )/8) . 'md',
                        // "value_id" =>$value->id,
                        "work_time"=> $reportsOfCategory->where('category_id', $value->id)->sum('cost')/60
                    ];
                } else {
                    $dataProcess [] = [
                        "label" => $value['name']. ' ~ ' .round( ( $reportsOfCategory->where('category_id', $value->id)->sum('cost') /60 )/8) . 'md',
                        // "value_id" =>$value->id,
                        "work_time"=> $reportsOfCategory->where('category_id', $value->id)->sum('cost')/60
                    ];
                }
            }

        }
        $dataCategory = $this->convertArraySort($dataCategory, 'work_time');
        $dataProcess = $this->convertArraySort($dataProcess, 'work_time');

        $data[] = [
            "label"                 => $project['name']. ' ~ ' .round(($reports->sum('cost')/60)/8) . 'md',
            "value"                 => 123,
            "project_quotation"     => $project['quotation_time'],
            "actual_work"           => $reports->sum('cost')/60
        ];

        $dataUsers = $modelProjectUser->getDetailuserProject($project_id);

        $dataUser = [];
        foreach ($userProject as $key => $value) {
            $dataUser[] = [
                "name"              => $dataUsers->where('user_id', $value->user_id)->first()->name . ' ~ ' .round( ( $reports->where('user_id', $value->user_id)->sum('cost') /60 )/8) . 'md' ,
                "project_quotation" => $value->project_quotation,
                "actual_work"       => $reports->where('user_id', $value->user_id)->sum('cost')/60,
            ];
        }
        $dataUser = $this->convertArraySort($dataUser, 'actual_work');
        return Response::json(array(
            'status'        =>'success',
            'data'          =>array(
                'data'          => $data,
                'cost_user'     => $data1,
                'category'      => $dataCategory,
                'process'       => $dataProcess,
                'user'          =>$dataUser,

            )
        ));

    }

    /**
     * @param array
     * $attr: element array oderby
     * @return array
     */
    private function convertArraySort($array, $attr) {
        $array = array_sort($array, function ($value) use ($attr){
            return $value[$attr];
        });
        $array = array_reverse($array);

        return $array;
    }
}
