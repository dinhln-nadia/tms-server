<?php namespace TMSApp\Http\Controllers;

use TMSApp\Http\Requests;
use TMSApp\Http\Controllers\Controller;

use Illuminate\Http\Request;
use TMSApp\Repositories\ReportRepository;
use TMSApp\Repositories\MissingReportRepository;
use TMSApp\Repositories\ReportCategoryRepository;
use TMSApp\Repositories\CategoryRepository;
use TMSApp\Repositories\UserRepository;
use TMSApp\Repositories\ProjectRepository;
use TMSApp\Repositories\SettingRepository;
use JWTAuth;
use HttpResponse;
use Exception;
use Response;
use Validator;
use Input;
use Carbon\Carbon;
use TMSApp\Models\Reports;
use Excel;
class ReportController extends Controller {

    private $now;
    private $report;
    private $relationship;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ReportRepository $report, ReportCategoryRepository $relationship)
    {
        $this->middleware('jwt.auth', ['except'=>[ 'exportExcel' ]]);
        $this->middleware('jwt.refresh', ['except'=>[ 'exportExcel' ]]);
        $this->report = $report;
        $this->relationship = $relationship;
        $this->now = Carbon::now();
    }

    /**
    * @param datetime $date
    */

    private function checkPreviewMonth($data)
    {
        $now = Carbon::now();
        $month = date("m", strtotime( $data) );
        $day = date("d", strtotime( $data ) );
        // if ( $now->month == $month ){
        //     return 0;
        // }
        if( $now->month == $month+1 ){
            if( $now->day > 5 ){
                return 1;
            }
            return 0;
        }
        if( $now->month > $month+1 ){
            return 1;
        }
        return 0;
    }

    /**
     * Create list time disable and enable of day
     * @param array $day
     * @param array $result
     */
    private function timeOfDay($day, $result)
    {
        if($result['end_time']-1 == $result['start_time']+1){
            $i = $result['end_time']-1;
            if ( isset( $day['enable'][$i] ) ){
                $day['disable'][$i] = $day['enable'][$i];
                unset($day['enable'][$i]);
            }
            return $day;
        }

        if ( $result['end_time']-1 == $result['start_time'] ) {

            if( $result['end_time'] == TMS_END_DAY ){
                $result['start_time']=  $result['start_time']+1;
            }elseif( $result['start_time'] == 0 ){
                $result['end_time']=  $result['end_time']-1;
            }else {
                return $day;
            }
        }

        if($result['end_time']-1 > $result['start_time']+1){
            if( $result['end_time'] == TMS_END_DAY ){
                $result['start_time']=  $result['start_time']+1;
            }elseif( $result['start_time'] == 0 ){
                $result['end_time']=  $result['end_time']-1;
            }else {
                $result['end_time'] = $result['end_time'] - 1;
                $result['start_time']=  $result['start_time'] + 1;
            }
        }

        for ($i=$result['start_time']; $i <= $result['end_time'] ; $i++) {
            if ( isset( $day['enable'][$i] ) ){
                $day['disable'][$i] = $day['enable'][$i];
                unset($day['enable'][$i]);
            }
        }

        return $day;
    }

    /**
     * @param  array $day
     * @param  array $time
     * @return array
     */
    private function getTimeKey($day, $time)
    {
        $start_time = date("H:i", strtotime( $time->start_time));
        $end_time = date("H:i", strtotime( $time->end_time));
        $result['start_time'] = array_search($start_time, $day);
        $result['end_time']= array_search($end_time, $day);
        return $result;
    }

    /**
     *
     * @param  ReportRepository $report
     * @return array
     */
    private function validatorTime( $result)
    {
        $time = [];
        $day = explode(',', TMS_TIME);
        foreach ($result as $key => $value) {
            $time[$key] = $this->getTimeKey($day, $value);
        }
        return $time;
    }

    /**
     * [removeTimeInReportUpdate description]
     * @param  array $day
     * @param  array $time
     * @return array
     */
    private function removeTimeInReportUpdate($day, $time)
    {
        $_day = explode(',', TMS_TIME);
        $value = $this->getTimeKey($_day, $time);
        for ($i=$value['start_time']; $i <= $value['end_time'] ; $i++) {
            if ( isset( $day['disable'][$i] ) ){
                $day['enable'][$i] = $day['disable'][$i];
                unset($day['disable'][$i]);
            }
        }
        return $day;
    }

    /**
     *  Return list time disable and enable
     * @param  ReportRepository $report
     * @return array
     */
    private function timeCanAddReport($report, $start_time, $item = null)
    {

        $year = date("Y", strtotime( $start_time));
        $month = date("m", strtotime( $start_time));
        $day = date("d", strtotime( $start_time));

        $day_number = Carbon::createFromDate($year, $month, $day);
        $user = JWTAuth::parseToken()->authenticate();
        $user_id=$user->id;
        $reportsInDay = $report->reportInDay($user_id, $day_number->day, $day_number);
        $result = $this->validatorTime($reportsInDay);
        $loop_count = count($result);
        $day = [];
        $day['enable'] = explode(',', TMS_TIME);
        for ($i=$loop_count-1; $i >= 0 ; $i--) {
            $day = $this->timeOfDay($day, $result[$i]);
        }
        if($item && date("Y-m-d", strtotime($item->start_time)) == date("Y-m-d", strtotime($start_time))){
            $day = $this->removeTimeInReportUpdate($day, $item);
        }
        return $day;
    }

    /**
     * @param  array $data
     * @param  array $time       start time , end time
     * @param  array $list_check
     * @return
     */
    private function checkTimeBetweenStartEnd( $data, $list_check, $report, $id = null)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $time['start'] = date("H:i", strtotime( $data['start_time']));
        $time['end'] = date("H:i", strtotime( $data['end_time']));
        $day = explode(',', TMS_TIME);

        $start = array_search( $time['start'], $day );
        $end = array_search( $time['end'], $day );

        // Remove lunch time.
        if($data['type'] == TMS_NORMAL_TYPE){
            for ($i=47; $i < 52; $i++) {
                if ( isset( $list_check['enable'][$i] ) ){
                    $list_check['disable'][$i] = $list_check['enable'][$i];
                    unset($list_check['enable'][$i]);
                }
            }
            //  xoá thời gian nhập daily report (Buổi tối)
            for ($i=71; $i <= 96; $i++) {
                if ( isset( $list_check['enable'][$i] ) ){
                    $list_check['disable'][$i] = $list_check['enable'][$i];
                    unset($list_check['enable'][$i]);
                }
            }
            //  xoá thời gian nhập daily report (Buổi sáng)
            for ($i=0; $i <= 31; $i++) {
                if ( isset( $list_check['enable'][$i] ) ){
                    $list_check['disable'][$i] = $list_check['enable'][$i];
                    unset($list_check['enable'][$i]);
                }
            }
        }

        if($end-1 == $start){
            $result = $report->findRerportByStarEnd($data['start_time'], $data['end_time'], $user->id );
            //
            if( $result->count('id') <> 0 ){
                // For update.
                if ( $result->count('id') == 1 && $result[0]->id == $id){
                    return 1;
                }
                return 0;
            }
        }
        if( in_array( $time['start'], $list_check['enable'] ) && in_array( $time['end'], $list_check['enable'] ) ){
            for ($i=$start; $i <=$end ; $i++) {
                if( !isset($list_check['enable'][$i]) ){ return 0; }
            }
            return 1;
        }
        return 0;
    }

    /**
     * Template of report
     * @param  array $value
     * @return response
     */
    private function templateReport($value)
    {
        $data['id']           = $value->id;
        $data['project_id']   = $value->project_id;
        $data['user_id']      = $value->user_id;
        $data['note']         = $value->note;
        $data['ticket']       = $value->ticket;
        $data['cost']         = $value->cost;
        $data['start_time']   = $value->start_time;
        $data['end_time']     = $value->end_time;
        $data['process_id']      = $value->process->first()->id;
        $data['category_id']     = $value->category->first()->id;
        $data['type']            = $value->type;

        return $data;
    }

    /**
     * Fill data with template
     * @param  collection $result
     * @param  array $categories
     * @return response
     */
    private function fillData($result, $categories = true)
    {
        $data = [];
        foreach($result as $key=>$value){
           $data[] = $this->templateReport($value);
        }
       return $data;
    }

    /**
    * Use to create new report.
    * @param array $input
    * @return mix
    */
    private function createNewReport($input)
    {
        // Create report
        try {
            // Create new row in table report.
            $data = $this->report->create($input);

            // Create new category relationship
            $input_category['category_id'] = $input['category_id'];
            $input_category['report_id'] = $data->id;
            $category = $this->relationship->create($input_category);
            // Create new process relationship
            $input_process['category_id'] =  $input['process_id'];
            $input_process['report_id'] = $data->id;
            $process = $this->relationship->create($input_process);
        } catch (\Exception $e) {
            // Remove data insert if fales
            if($data){
                $this->report->find($data->id)->forceDelete();
            }
            if($category){
                $this->relationship->find($category->id)->forceDelete();
            }
            if($process){
                $this->relationship->find($process->id)->forceDelete();
            }

            throw new Exception( $e->getMessage() );
        }
        return $data;
    }

    /**
     * Fill data with template
     * @param array $result
     * @param  array $categories
     * @return response
     */
    private function missingReportTemplate($result, $loop){
        $data = [];
        $data['user_id'] = $result['user_id'];
        for ($i=1; $i < $loop; $i++) {
            $isWeekend = Carbon::createFromDate($result->year, $result->month, $i)->isWeekend();
            $thisDayColumn = 'day_'.$i;
            if( !$isWeekend ){
                if($result[$thisDayColumn] == TMS_MISSING){
                    $data['day_forget'][] = $i;
                }
                if($result[$thisDayColumn] == TMS_DAYOFF){
                    $data['day_off'][] = $i;
                }
            }
        }
        return $data;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(ReportRepository $model)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $result = $model->getAll($user);

        $data = $this->fillData($result);
        $status = TMS_STATUS;

        return Response::json(compact('status', 'data') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ReportCategoryRepository $relationship, ReportRepository $report, CategoryRepository $categories, MissingReportRepository $modelMissingReport)
    {
        $data = Input::all();
        $user = JWTAuth::parseToken()->authenticate();
        // Create validator
        $messages = [
            'ticket.regex'    =>  trans('message.validate.ticket.regex'),
        ];
        $validator = Validator::make($data, [
            'project_id'    => 'required|integer',
            'ticket'        => array('regex:' . TMS_TICKET_REGEX_VALIDATE, 'max:8'),// + TMS_TICKET_REGEX_VALIDATE),/^[0-9]+$/

            'start_time'    => 'required|date',
            'end_time'      => 'required|date|after:start_time',
            'category_id'   => 'required|integer',
            'process_id'    => 'required|integer',
            'note'          => 'required|max:255',
            'type'          => 'required|integer',

        ], $messages);
        // return Response::json(['create']);

        // Thorw message error if fail.
        if ($validator->fails()) {
            return Response::json(['error' => $validator->messages()], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        // Thorw message error if start_time and end_time not in day time list.
        $day = explode(',', TMS_TIME);
        $time['start'] = date("H:i", strtotime( $data['start_time']));
        $time['end'] = date("H:i", strtotime( $data['end_time']));

        $list_check = $this->timeCanAddReport($report, $data['end_time']);

        if ( !(in_array($time['start'], $day) ) ){
            return Response::json(['error' => trans('message.time_error')], HttpResponse::HTTP_CONFLICT);
        }
        if ( !(in_array($time['end'], $day) ) ){
            return Response::json(['error' => trans('message.time_error')], HttpResponse::HTTP_CONFLICT);
        }
        if( !$this->checkTimeBetweenStartEnd($data, $list_check, $report) ){
            return Response::json(['error' => trans('message.time_error')], HttpResponse::HTTP_CONFLICT);
        }

        // Thorw message error if date of start_time and end_time not equal.
        $start_time = date("Y/m/d", strtotime( $data['start_time']));
        $end_time = date("Y/m/d", strtotime( $data['end_time']));
        if ( !($start_time === $end_time) ){
            return Response::json(['error' => trans('message.time')], HttpResponse::HTTP_CONFLICT);
        }
        //
        if( $data['category_id'] === $data['process_id'] ){
            return Response::json(['error' => trans('report.report_category_dublicate')], HttpResponse::HTTP_CONFLICT);
        }

        $category_type = $categories->find( $data['category_id'] );
        // If input isn't category type then return error
        if( !($category_type->type == TMS_CATEGORY) ){
            return Response::json(['error' => trans('message.report_category_exit')], HttpResponse::HTTP_CONFLICT);
        }

        $process_type = $categories->find( $data['process_id'] );
        // If input isn't category type then return error
        if( !($process_type->type == TMS_PROCESS) ){
            return Response::json(['error' => trans('message.report_process_exit')], HttpResponse::HTTP_CONFLICT);
        }
        try {
            $data = [];
            $input_process= [];
            $input_category= [];
            $input = Input::except('category','process');

            $input['user_id'] = $user->id;
            // Calculation cost of report.
            $datetime1 = strtotime( $input['start_time']);
            $datetime2 = strtotime( $input['end_time']);
            // Seconds between the two times
            $secs = $datetime2 - $datetime1;
            //
            $check_end_of_day = date("H:i", strtotime( $input['end_time'])+1);
            if($check_end_of_day == '23:59'){
                $secs= $secs+60;
            }
            $input['cost'] = $secs / 60;

            // Create new row in table report.
            $data = $report->create($input);

            // Create new category relationship
            $input_category['category_id'] = Input::get('category_id');
            $input_category['report_id'] = $data->id;
            $category = $relationship->create($input_category);
            // Create new process relationship
            $input_process['category_id'] =  Input::get('process_id');
            $input_process['report_id'] = $data->id;
            $process = $relationship->create($input_process);

        } catch (\Exception $e) {
            // return $e->getMessage();
            return Response::json(['error' => trans('message.report_exists')], HttpResponse::HTTP_CONFLICT);
        }
        $data['process_id'] = $process->category_id;
        $data['category_id'] = $category->category_id;
        $status= TMS_STATUS;


        /***
         * Install table missingreport
         * If this day = input day => don't user this function
         */
        $dayMissing = 0;

        $time = $this->formatTimeInstall ( $input['start_time'] );
        $now = Carbon::now();
        if($now->day <= $time['time']['day'] && $now->month == $time['time']['month']) {
            $dayMissing = ['status'=>1];
            return Response::json(compact('status', 'data', 'dayMissing'));

        }

        $checkCost = $report->checkCost( $time , $user->id);
        $day = 'day_'.intval($time['time']['day']);
        if($checkCost) {
            $dayMissing = ['day' => intval($time['time']['day']), 'month' => $time['time']['month'], 'status' => TMS_REPORT_DONE ];
            $modelMissingReport->updateMissing($user->id, $time['time']['year'], $time['time']['month'], $day);

            return Response::json(compact('status', 'data','dayMissing'));
        }


        $dayMissing = ['day' => intval($time['time']['day']), 'month' => $time['time']['month'], 'status' => TMS_NOT_DAYOFF ];
        $modelMissingReport->updateMissingNG($user->id, $time['time']['year'], $time['time']['month'], $day);
        return Response::json(compact('status', 'data','dayMissing'));

    }

    /**
     * Format time -
     */
    private function formatTimeInstall ( $time ) {
        $dataTime = $this->formatTime( $time );
        $dataStartTime = $dataTime['year'].'-'.$dataTime['month'].'-'.$dataTime['day'].' '.'01:00:00';
        $dataEndTime = $dataTime['year'].'-'.$dataTime['month'].'-'.$dataTime['day'].' '.'23:59:00';
        return $data = [ 'start' => $dataStartTime, 'end'   => $dataEndTime, 'time' => $dataTime];
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function missingReport(MissingReportRepository $report, UserRepository $UserModel)
    {
        $data = Input::all();
        $user = JWTAuth::parseToken()->authenticate();
        // Create validator
        $validator = Validator::make($data, [
            'month' => 'integer',
            'year' => 'integer',
        ]);
        $listUserDelete = $UserModel->getUserOlyTrash();
        // Thorw message error if fail.
        if ($validator->fails()) {
            return Response::json(['error' => $validator->messages()], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        $month  = isset($data['month']) ?  $data['month'] : 0;
        $year   = isset($data['year']) ?  $data['year'] : 0;
        $result = $report->missingReport($month, $year,null, $listUserDelete);
        $data = [];
        $now = Carbon::now();
        if(!$result->isEmpty()){
            if($month == 0 || $month == $now->month){
                $toDay = $now->day;
            }else {
                $now = Carbon::createFromDate($result->first()->year, $result->first()->month,1);
                $toDay = $now->daysInMonth;
            }

            foreach ($result as $key => $value) {
                $data[] = $this->missingReportTemplate($value, $toDay);
            }
        }

        $status= TMS_STATUS;
        return Response::json(compact('status', 'data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, ReportCategoryRepository $relationship, ReportRepository $report, CategoryRepository $categories, MissingReportRepository $modelMissingReport)
    {
        $data = Input::get();
        $user = JWTAuth::parseToken()->authenticate();

        $messages = [
            'ticket.regex'    =>  trans('message.validate.ticket.regex'),
            'type.not_in'     =>  trans('message.error.admin_accepted')
        ];
        // Create validator
        $validator = Validator::make($data, [
            'project_id'    => 'integer',
            'ticket'        => array('regex:' . TMS_TICKET_REGEX_VALIDATE, 'max:8'),// + TMS_TICKET_REGEX_VALIDATE),/^[0-9]+$/

            'start_time'    => 'date',
            'end_time'      => 'date|after:start_time',
            'category_id'   => 'required|integer',
            'process_id'    => 'required|integer',
            'note'          => 'max:255',
            'type'          => 'required|integer|not_in:' . TMS_OT_ACCEPT_TYPE . ',' . TMS_ABSENCE_ACCEPT_TYPE,

        ], $messages);

        // Thorw message error if fail.
        if ($validator->fails()) {

            return Response::json(['error' => $validator->messages()], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        // Check status of report in database is accepted (TMS_OT_ACCEPT_TYPE, TMS_ABSENCE_ACCEPT_TYPE) ?
        if(!$id){

            return Response::json(['error' => trans('message.report_is_empty')], HttpResponse::HTTP_CONFLICT);
        }

        $report_accept = $this->report->find($id);

        if(empty($report_accept)){

            return Response::json(['error' => trans('message.report_is_empty')], HttpResponse::HTTP_CONFLICT);
        }

        if($report_accept->type == TMS_OT_ACCEPT_TYPE || $report_accept->type == TMS_ABSENCE_ACCEPT_TYPE){

            return Response::json(['error' => trans('message.error.admin_accepted')], HttpResponse::HTTP_CONFLICT);
        }

         // Thorw message error if start_time and end_time not in day time list.
        $day = explode(',', TMS_TIME);
        $time['start'] = date("H:i", strtotime( $data['start_time']));
        $time['end'] = date("H:i", strtotime( $data['end_time']));
        $item = $report->find($id,['created_at', 'start_time', 'end_time']);
        // Thorw message error if find report non-object.
        if($item){
            $list_check = $this->timeCanAddReport($report, $data['start_time'], $item);
        }else{
            return Response::json(['error' => trans('message.report_is_empty')], HttpResponse::HTTP_CONFLICT);
        }
        // Throw message error if edit report on preview month.
        if( $this->checkPreviewMonth($data['start_time']) ){
            return Response::json(['error' => trans('message.update_error')], HttpResponse::HTTP_CONFLICT);
        }
        if ( !(in_array($time['start'], $day) ) ){
            return Response::json(['error' => trans('message.time_error')], HttpResponse::HTTP_CONFLICT);
        }
        if ( !(in_array($time['end'], $day) ) ){
            return Response::json(['error' => trans('message.time_error')], HttpResponse::HTTP_CONFLICT);
        }
        if( !$this->checkTimeBetweenStartEnd($data, $list_check, $report, $id) ){
            return Response::json(['error' => trans('message.time_error')], HttpResponse::HTTP_CONFLICT);
        }

        // Thorw message error if date of start_time and end_time not equal.
        $start_time = date("Y/m/d", strtotime( $data['start_time']));
        $end_time = date("Y/m/d", strtotime( $data['end_time']));

        if ( !($start_time === $end_time) ){
            return Response::json(['error' => trans('message.time')], HttpResponse::HTTP_CONFLICT);
        }

        // Return message error if catgory_id equal process_id.
        if( $data['category_id'] === $data['process_id'] ){
            return Response::json(['error' => trans('message.report_category_dublicate')], HttpResponse::HTTP_CONFLICT);
        }

        $category_type = $categories->find( $data['category_id'] );
        // If input isn't category type then return error
        if( !($category_type->type == TMS_CATEGORY) ){
            return Response::json(['error' => trans('message.report_category_exit')], HttpResponse::HTTP_CONFLICT);
        }

        $process_type = $categories->find( $data['process_id'] );
        // If input isn't category type then return error
        if( !($process_type->type == TMS_PROCESS) ){
            return Response::json(['error' => trans('message.report_process_exit')], HttpResponse::HTTP_CONFLICT);
        }

        $input = Input::except('category_id', 'process_id', 'user_id', 'cost');
         // If insset start_time and end_time
        if( isset($input['start_time']) && isset($input['end_time']) ){
            // Calculation cost of report.
            $datetime1 = strtotime( $input['start_time']);
            $datetime2 = strtotime( $input['end_time']);
            //Seconds between the two times
            $secs = $datetime2 - $datetime1;
            //
            $check_end_of_day = date("H:i", strtotime( $input['end_time'])+1);
            if($check_end_of_day == '23:59'){
                $secs= $secs+60;
            }
            $input['cost'] = $secs / 60;
        }
        try {
            if(isset($input['date'])){
                unset($input['date']);
            }
            // Update row in table report
            $report->update($input, $id);

            // Delete all Category relationship with id
            $relationship->delete($id);
            // Create new category relationship
            $input_category['category_id'] = Input::get('category_id');
            $input_category['report_id'] = $id;
            $category = $relationship->create($input_category);
            // Create new process relationship
            $input_process['category_id'] =  Input::get('process_id');
            $input_process['report_id'] = $id;
            $process = $relationship->create($input_process);
        } catch (\Exception $e) {
            return Response::json(['error' => trans('message.report_category')], HttpResponse::HTTP_CONFLICT);
        }
        $result = $report->find($id);
        $data = $this->templateReport($result);
        $status= TMS_STATUS;

        /***
         * Install table missingreport
         * If this day = input day => don't user this function
         */
        $dayMissing = [];

        $time = $this->formatTimeInstall ( $input['start_time'] );
        $now = Carbon::now();
        if($now->day <= $time['time']['day'] && $now->month == $time['time']['month']) {
            $dayMissing = ['status'=>1];
            return Response::json(compact('status', 'data', 'dayMissing'));

        }

        $checkCost = $report->checkCost( $time , $user->id);
        $day = 'day_'.intval($time['time']['day']);
        if($checkCost) {
            $dayMissing = ['day' => intval($time['time']['day']), 'month' => $time['time']['month'], 'status' => TMS_REPORT_DONE ];
            $modelMissingReport->updateMissing($user->id, $time['time']['year'], $time['time']['month'], $day);

            return Response::json(compact('status', 'data','dayMissing'));
        }


        $dayMissing = ['day' => intval($time['time']['day']), 'month' => $time['time']['month'], 'status' => TMS_NOT_DAYOFF ];
        $modelMissingReport->updateMissingNG($user->id, $time['time']['year'], $time['time']['month'], $day);
        return Response::json(compact('status', 'data','dayMissing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function updateMissingReport( MissingReportRepository $missing_report)
    {
        $data = Input::get();
        $user = JWTAuth::parseToken()->authenticate();

        $rules = [
            'day_off'   => 'required|integer|max:31|min:1',
            'month'     => 'integer|max:12',
            'year'      => 'integer|min:' . $this->now->year
        ];
        // Create validator
        $validator = Validator::make($data, $rules);

        // Thorw message error if fail.
        if ($validator->fails()) {
            return Response::json(['error' => $validator->messages()], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        $dayoff_reports = [];
        try {
            // Check month and year valid ?
            $data = $this->checkDayoffDayValid($data);

            // Check report in day. If it is null can update day off.
            $this->checkDayoffReportInDay($data, $user);

            // Create report type workoff.
            $dayoff_reports = $this->createDayoffReport($data, $user);

            // Update missing table.
            $result = $this->updateDayoffMissingTable($data, $user, $missing_report);

        } catch (Exception $e) {
            // handle if error

            return $this->handleErrorDayoffReport($e);
        }

        $status = TMS_STATUS;
        $data = $dayoff_reports;

        return Response::json(compact('status', 'data'));
    }

    /**
     * Function check month and year is valid for dayoff
     * @param  Array    $data    Input received from client
     * @return Array             Array data updated
     */
    private function checkDayoffDayValid($data){
        // If field month is null month = now month.
        if( !isset($data['month']) ){
            $data['month'] = $this->now->month;
        } else if($data['month'] < ($this->now->month - 1)) {

            throw new Exception('dayoff error', TMS_THROWN_DAYOFF_DAYINVALID);
        }

        // If field year is null year = now year.
        if(!isset($data['year'])){
            $data['year'] = $this->now->year;

            return $data;
        }

        if( empty($data['year'])){
            $data['year'] = $this->now->year;
        }

        return $data;
    }

    /**
     * Function to check Report in day
     * @param  Array        $data    Input received from client
     * @param  Object       $user    Information of current user
     */
    private function checkDayoffReportInDay($data, $user){
        // Check report in day. If it is null can update day off.
        $day_number = Carbon::createFromDate($data['year'], $data['month'], $data['day_off']);
        $time = [
            'start'=>" 08:00:00",
            'end'  =>" 17:30:00"
        ];
        $reportsInDay = $this->report->reportInDay($user->id, $day_number->day, $day_number, $time);

        if($reportsInDay->count()){

            throw new Exception('dayoff error', TMS_THROWN_DAYOFF_REPORTINDAY);
        }
    }


    /**
     * Function to create 2 report of dayoff
     * @param  Array    $data   Input recieved from client
     * @param  Object   $user   Information of current user
     * @return Array            Array data of 2 report insert success to database
     */
    private function createDayoffReport($data, $user){

        // report in the moring.
        $start_time_morning = Carbon::create($data['year'], $data['month'], $data['day_off'], 8, 00, 00, TMS_TIME_ZONE);
        $end_time_morning = Carbon::create($data['year'], $data['month'], $data['day_off'], 11, 30, 00, TMS_TIME_ZONE);
        $dayoff_reports[0] = $this->createSessionDayoffReport($user, $start_time_morning, $end_time_morning, TMS_MORNING_SESSION);

        //report in the afternoon.
        $start_time_afternoon = Carbon::create($data['year'], $data['month'], $data['day_off'], 13, 00, 00, TMS_TIME_ZONE);
        $end_time_afternoon = Carbon::create($data['year'], $data['month'], $data['day_off'], 17, 30, 00, TMS_TIME_ZONE);
        $dayoff_reports[1] = $this->createSessionDayoffReport($user, $start_time_afternoon, $end_time_afternoon, TMS_AFTERNOON_SESSION);

        return $dayoff_reports;
    }


    /**
     * Function to update missing table
     * @param  Array $data                                  Input received from client
     * @param  Object $user                                 Information of current user
     * @param  MissingReportRepository  $missing_report     Missing table object
     * @return Report                                       Report create success in database
     */
    private function updateDayoffMissingTable($data, $user, $missing_report){
        // Update missing table.
        $day = 'day_'.$data['day_off'];
        $result[$day] = TMS_DAYOFF;
        $pattern = array('month'    => $data['month'],
                         'year'     => $data['year'],
                         'user_id'  => $user->id);

        return $missing_report->update($result, $pattern);
    }


    /**
     * Function to create a session (morning or afternoon) of dayoff report
     * @param  Object   $user       Information of current user
     * @param  DateTime $start_time Start time of report
     * @param  DateTime $end_time   End time of report
     * @param  Integer  $time       Minutes of cost
     * @return Report               Report create success in database
     */
    private function createSessionDayoffReport($user, $start_time, $end_time, $time){

        $report_morning =[
            'project_id' => 1,
            'ticket' => 0,
            'cost' => $time,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'category_id' => TMS_CATEGORY_NULL, //1,
            'process_id' => TMS_PROCESS_NULL,   //2,
            'user_id'   => $user->id,
            'note' => TMS_NOTE_DAYOFF,
            'type'  =>  TMS_WORKOFF_TYPE,
        ];

        $dayoff_reports = $this->createNewReport($report_morning);
        $dayoff_reports['start_time'] = $dayoff_reports['start_time']->toDateTimeString();
        $dayoff_reports['end_time'] = $dayoff_reports['end_time']->toDateTimeString();

        return $dayoff_reports;
    }

    /**
     * Handle exception thrown from create dayoff
     * @param  Exception $e     Exception thrown
     * @return JSON
     */
    public function handleErrorDayoffReport(Exception $e) {
        $code = $e->getCode();

        if($code == TMS_THROWN_DAYOFF_DAYINVALID){
            $error_msg = ['error' => trans('message.dayoff.error.month_invalid')];
        } elseif ($code == TMS_THROWN_DAYOFF_REPORTINDAY) {
            $error_msg = ['error' => trans('message.update_missing_error')];
        } else {
            $error_msg = ['error' => trans('message.update_error')];
            // $error_msg = ['error' => $e->getMessage()];
        }

        return Response::json($error_msg, HttpResponse::HTTP_NOT_ACCEPTABLE);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id,  ReportCategoryRepository $relationship, ReportRepository $report, MissingReportRepository $modelMissingReport)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $reportOfUser = $report->find($id);

        // If report is empty return error mesage
        if ( empty($reportOfUser) ){
            return Response::json(['error' => trans('message.report_is_empty')], HttpResponse::HTTP_CONFLICT);
        }

        // Check status of report in database is accepted (TMS_OT_ACCEPT_TYPE, TMS_ABSENCE_ACCEPT_TYPE) ?
        if($reportOfUser->type == TMS_OT_ACCEPT_TYPE || $reportOfUser->type == TMS_ABSENCE_ACCEPT_TYPE){

            return Response::json(['error' => trans('message.error.admin_accepted')], HttpResponse::HTTP_CONFLICT);
        }

        // If permission of user is admin or this is report of user request then We can delete it.
        if ($user->isAdmin() || $reportOfUser->user_id === $user->id){

            try {
                // Delete all Category relationship with id
                $relationship->delete($id);

                // Delete row in table report
                $report->delete( $id);
            } catch (\Exception $e) {
                return Response::json(['error' => trans('message.report_category')], HttpResponse::HTTP_CONFLICT);
            }

            $status= TMS_STATUS;

            $dayMissing = $this->checkRealtime( $report, $reportOfUser->start_time, $user, $modelMissingReport);
            $data[] = $dayMissing;
            return Response::json(compact('status', 'data'));

        }

        return Response::json(['error' => trans('message.permission')], HttpResponse::HTTP_CONFLICT);
    }

    /**
     * Check cost report in day
     */
    private function checkRealtime ( $report, $timeCheck, $user, $modelMissingReport ) {

            $dayMissing = 0;
            $now = Carbon::now();
            $time = $this->formatTimeInstall ( $timeCheck );
            $dayMissing = [];
            if($now->day <= $time['time']['day'] && $now->month == $time['time']['month']) {
                $dayMissing = ['status'=>1];
                return $dayMissing;
            }

            $checkCost = $report->checkCost( $time , $user->id);
            $day = 'day_'.intval($time['time']['day']);
            if($checkCost) {
                $dayMissing = ['day' => intval($time['time']['day']), 'month' => $time['time']['month'], 'status' => TMS_REPORT_DONE ];
                $modelMissingReport->updateMissing($user->id, $time['time']['year'], $time['time']['month'], $day);

                return $dayMissing;
            }

            $dayMissing = ['day' => intval($time['time']['day']), 'month' => $time['time']['month'], 'status' => TMS_NOT_DAYOFF ];
            $modelMissingReport->updateMissingNG($user->id, $time['time']['year'], $time['time']['month'], $day);
            return $dayMissing;
    }

    /**
     * Search the specified resource from storage.
     *
     * @param  string  $name
     * @return Response
     */
    public function search(ReportRepository $model)
    {
        $data = Input::get();
        $user = JWTAuth::parseToken()->authenticate();
        // Create validator
        $messages = [
            'ticket.regex'    =>  trans('message.validate.ticket.regex'),
        ];
        $validator = Validator::make($data, [
            'project_id' => 'integer',
            // 'ticket' => array('regex:' . TMS_TICKET_REGEX_VALIDATE),// + TMS_TICKET_REGEX_VALIDATE),/^[0-9]+$/

            'start_time' => 'date',
            'end_time' => 'date|after:start_time',
            'category_id' => 'integer',
            'process_id' => 'integer',
            'user_id' => 'integer',
            'type'  =>  'integer',

        ], $messages);
        // If validation fails return error message.
        if ($validator->fails()) {
            return Response::json(['error' => $validator->messages()], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        $time = $this->checkTimeSearch($data);
        $categories = $this->checkCategorySearch($data);
        $pattern = Input::except('start_time','end_time', 'category_id', 'process_id', 'page', 'option', 'ticket', 'count','key_note');
        $result = $this->searchWithOption(Input::all(), $user, $pattern, $time, $categories, $data);
        $data = $this->fillData($result);
        $status = TMS_STATUS;

        return Response::json(compact('status', 'data'));
    }

    /**
     * Check category for search
     *
     *
     * @return array
     */
    private function checkCategorySearch($data){

        $categories = [];
        $categories['category'] = isset($data['category_id']) ?  $data['category_id'] : null;
        $categories['process']  = isset($data['process_id']) ?  $data['process_id'] : null;

        return $categories;
    }

    /**
     * Check time for search
     *
     *
     * @return array
     */
    private function checkTimeSearch($data){

        $thisDay = Carbon::now();
        $preDay = "2015-06-20 23:59:00.000";

        $time['preDay']     = isset($data['start_time']) ?  $data['start_time'] : $preDay;
        $time['thisDay']    = isset($data['end_time']) ?  $data['end_time'] : $thisDay;
        return $time;
    }

    /**
     * Search with parameter
     *
     *
     * @return LengthAwarePaginator Object  -- Object of paginate
     */
    private function searchWithOption($input, $user, $pattern, $time, $categories, $data) {
        $option = isset($input['option']) ? $input['option'] : null;
        $isAdmin = $user->isAdmin();

        $filter_type = null;
        switch ($option) {
            case null:

                break;

            case TMS_OPTION_OVERTIME_MANAGERMENT:

                if($isAdmin){
                    $filter_type = [TMS_OT_TYPE, TMS_OT_ACCEPT_TYPE];
                } else {
                    $pattern['user_id'] = $user->id;
                    $filter_type = [TMS_OT_TYPE, TMS_OT_ACCEPT_TYPE, TMS_OT_REJECT_TYPE];
                }

                break;

            case TMS_OPTION_OVERTIME_REPORT:

                $pattern['user_id'] = $user->id;
                $filter_type = [TMS_OT_TYPE, TMS_OT_ACCEPT_TYPE, TMS_OT_REJECT_TYPE];

                break;

            case TMS_OPTION_ABSENCE_REPORT:
                if($isAdmin){
                    $filter_type = [TMS_WORKOFF_TYPE, TMS_ABSENCE_ACCEPT_TYPE];
                } else {
                    $pattern['user_id'] = $user->id;
                    $filter_type = [TMS_WORKOFF_TYPE, TMS_ABSENCE_ACCEPT_TYPE, TMS_ABSENCE_REJECT_TYPE];
                }

                break;

            case TMS_OPTION_ABSENCE_MANAGERMENT:
                if(! is_null($input['user_id'])){    //Input::has('user_id')) {
                    $pattern['user_id'] = Input::get('user_id');
                }

                $filter_type = [TMS_WORKOFF_TYPE, TMS_ABSENCE_ACCEPT_TYPE, TMS_ABSENCE_REJECT_TYPE];

                break;

            default:

                break;
        }

        $page = empty($input['page']) ? null : $input['page'];

        return $this->report->search($user, $pattern, $time, $categories, $data, $filter_type, $page);
    }


    /**
     * Change accept overtime report.
     * @param int $id       ID of report must reject
     *
     * @return json
     */
    public function acceptOvertime($id) {

        return $this->changeStatusReport($id, TMS_OT_ACCEPT_TYPE);
    }

    /**
     * Change reject overtime report.
     * @param int $id       ID of report must reject
     *
     * @return json
     */
    public function rejectOvertime($id) {

        return $this->changeStatusReport($id, TMS_OT_REJECT_TYPE);
    }

    /**
     * Change accept absence report.
     * @param int $id       ID of report must reject
     *
     * @return json
     */
    public function acceptAbsence($id) {

        return $this->changeStatusReport($id, TMS_ABSENCE_ACCEPT_TYPE);
    }

    /**
     * Change reject absence report.
     * @param int $id       ID of report must reject
     *
     * @return json
     */
    public function rejectAbsence($id) {

        return $this->changeStatusReport($id, TMS_ABSENCE_REJECT_TYPE);
    }

    /**
     * Change status overtime report.
     * @param int $id       ID of report need change status
     * @param int $type     Status need set
     *
     * @return json
     */
    private function changeStatusReport($id, $type){
        // Check permission is Admin
        $user = JWTAuth::parseToken()->authenticate();
        if(! $user->isAdmin()){

            return Response::json(['error' => trans('message.require_admin')], HttpResponse::HTTP_CONFLICT);
        }

        // Change status report to  $type
        return $this->updateStatusReport($id, $type);
    }

    /**
     * Update status overtime report in database
     * @param int $id       ID of report need change status
     * @param int $type     Status need set
     *
     * @return json
     */
    private function updateStatusReport($id, $type){
        try{
            // update type of report
            $result = $this->report->update(['type' =>  $type], $id);
        } catch (\Exception $e) {

            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_CONFLICT);
        }

        $status = TMS_STATUS;
        $data = $this->report->find($id);

        return Response::json(compact('status', 'data'));
    }

    /**
     * @param start time and end time
     * @return response
     */
    public function duplicateDay(ReportRepository $model, ReportCategoryRepository $relationship, MissingReportRepository $modelMissingReport) {
        $user = JWTAuth::parseToken()->authenticate();
        $dataInput = Input::all();
        // get date duplicate
        $oldTime = $this->formatTimeOld ($dataInput);
        // Check report in day
        $result = $model->getAllReportInDay($dataInput, $user->id);

        if ($result->count() > 0) {

            return Response::json(['error' => trans('Please remove all reports this day before using duplicate day function')], HttpResponse::HTTP_CONFLICT);

        }

        $validateTime_duplicate = $this->formatTime($dataInput['start_time']);

        $dt = Carbon::createFromDate($validateTime_duplicate['year'], $validateTime_duplicate['month'], $validateTime_duplicate['day']);

        if( $validateTime_duplicate['day'] == 1 ) {

            return Response::json(['error' => trans(' can`t use this function for the first day of month. ')], HttpResponse::HTTP_CONFLICT);

        }

        if( $validateTime_duplicate['day'] == 2 & $dt->dayOfWeek === Carbon::MONDAY || $validateTime_duplicate['day'] == 3 & $dt->dayOfWeek === Carbon::MONDAY) {

            return Response::json(['error' => trans(' can`t use this function for the first day of May. ')], HttpResponse::HTTP_CONFLICT);

        }

        // get data duplicate
        $dataOldDay = $result = $model->getAllReportLastDay($oldTime, $user->id);
        if ($dataOldDay->count() == 0) {
            return Response::json(['error' => trans('Yesterday null data !')], HttpResponse::HTTP_CONFLICT);
        }

        foreach ($dataOldDay as $value) {
            $dataInstall = [
               'project_id' =>  $value->project_id,
               'user_id'    =>  $value->user_id,
               'note'       =>  $value->note,
               'ticket'     =>  $value->ticket,
               'cost'       =>  $value->cost,
               'start_time' =>  $this->formatThisTime($value->start_time),
               'end_time'   =>  $this->formatThisTime($value->end_time),
               'category_id'=>  $this->getIdCategory($value->category),
               'process_id' =>  $this->getIdCategory($value->process),
               'type'       =>  0
            ];

            $this->installDataDuplicate($dataInstall ,$model, $relationship);
        }
        $status= TMS_STATUS;
        $dayMissing = $this-> checkRealtime( $model, $dataInstall['start_time'], $user, $modelMissingReport );

        return Response::json(compact('status','dayMissing'));
    }


    /**
     * Install data
     */
    private function installDataDuplicate($dataInstall, $report, $relationship) {

        // get category and process_id before delete
        $input_category['category_id'] = $dataInstall['category_id'];
        $input_process['category_id']  = $dataInstall['process_id'];

        unset($dataInstall['category_id']);
        unset($dataInstall['process_id']);

        // Create new row in table report.
        $data = $report->create($dataInstall);

        // Create new category relationship
        $input_category['report_id'] = $data->id;
        $category = $relationship->create($input_category);
        // Create new process relationship
        $input_process['report_id'] = $data->id;
        $process = $relationship->create($input_process);

    }

    private function getIdCategory($category) {
        foreach ($category as $value) {
            $category_id = $value->id;
        }
        return $category_id;
    }

    private function formatTime ($time)
    {
        $year   = substr( $time, 0, 4 );

        $month  = substr( $time, 5, 2 );

        $day    = substr( $time, 8, 2 );

        return array(
            'year' => $year,
            'month'=> $month,
            'day'  => $day
        );
    }

    /**
     * Format Old time
     */
    private function formatTimeOld ($data) {

        $year   = substr($data['start_time'],0,4);

        $month  = substr($data['start_time'],5,2);

        $day = substr($data['start_time'],8,2);

        $dt = Carbon::createFromDate($year, $month, $day);

        if ($dt->dayOfWeek === Carbon::MONDAY) {

            $oldDay = substr($data['start_time'],8,2) - 3;

        } else {

            $oldDay = substr($data['start_time'],8,2) - 1;

        }

        $data['start_time'] = $year .'-'. $month .'-'.$oldDay.' '.'00:00:01';

        $data['end_time'] = $year .'-'. $month .'-'.$oldDay.' '.'23:59:00';

        return $data;
    }

    /**
     * For mat new time
     */
    private function formatThisTime ($data) {

        $year   = substr($data,0,4);

        $month  = substr($data,5,2);

        $day = substr($data,8,2);

        $house = substr($data,11,8);

        $dt = Carbon::createFromDate($year, $month, $day);

        if ($dt->dayOfWeek === Carbon::FRIDAY) {

            $oldDay = substr($data,8,2) + 3;

        } else {

            $oldDay = substr($data,8,2) + 1;

        }

        $data = $year .'-'. $month .'-'.$oldDay.' '.$house;

        return $data;
    }

    /**
     * Format Month
     */
    private function formatTimeMonth ($month) {
        $dt = Carbon::now();
        if($month == 1) {
            $lastMonth = ($dt->year - 1).'-'.'12'.'-'.'21'.' 00:00:00';
        }
        else {
            $lastMonth = $dt->year.'-'.($month - 1).'-'.'21'.' 00:00:00';
        }
        $thisMonth = $dt->year.'-'.$month.'-'.'20'.' 23:59:00';
        $data = ['ThisMonth' => $thisMonth, 'lastMonth' => $lastMonth ];

        return $data;
    }

    /**
     * Get Data for excel files
     */

    private function getDataForFileExcel ( $month, $userModel, $reportModel, $projectModel, $settingModel ) {
        $now = Carbon::now();

        if( $month < 1 || $month > 12 || $now->month < $month ) {
            throw new Exception('Month incorrect!');
        }

        $month = $this->formatTimeMonth ($month);

        $listIdUser = $userModel->getListID();
        $data = $this->getDataOTReport($listIdUser, $month, $reportModel);

        foreach ($data as $values) {
            $stt = 0;
            foreach ($values as  $value) {
                $user_id = $value['user_id'];
                $name = $userModel->getName( $user_id );
                $dataNgayNghi = $this->ValidateOverTime($settingModel, substr($value['start_time'], 0,10));
                $dataExcel[$name][] = [
                    'STT' => $stt = $stt + 1,
                    'Ngày thực hiện' => substr($value['start_time'], 0,10),
                    'Bắt đầu'        => substr($value['start_time'], 11,11),
                    'Kết thúc '      => substr($value['end_time'], 11,11),
                    'Lý do đăng ký'  => $value['note'],
                    'Dự án'          => $projectModel->getNameProject($value['project_id']),
                    'Số giờ'         => $value['cost']/60,
                    'Ngày thường'    => $dataNgayNghi['ngaythuong'],
                    'Ngày nghỉ'      => $dataNgayNghi['ngaynghi'],
                    'Ngày lễ'        => $dataNgayNghi['ngayle'],
                ];

            }
        }

        return $dataExcel;
    }
    /**
     * Validate time Overtime
     */
    private function ValidateOverTime ($settingModel, $time) {

        $dataNgayNghi = array(
            'ngaythuong' => '',
            'ngaynghi'   => '',
            'ngayle'     => ''
            );
        $time = $time.' 00:00:00';
        $dt = Carbon::parse($time);
        $listDayOff = $settingModel->getlist();
        $data = unserialize($listDayOff->setting_value);
        foreach ($data as $value) {
            if($time == $value['day']) {
                $dataNgayNghi['ngayle'] = 'O';
                break;
            } else if( $dt->isWeekend() ) {
                $dataNgayNghi['ngaynghi'] = 'O';
            } else {
                $dataNgayNghi['ngaythuong'] = 'O';
            }
        }
        return $dataNgayNghi;
    }

    /**
     * Get report for users
     * @param collection users id
     * @return data Over time report all user
     */
    private function getDataOTReport($listIdUser, $month, $reportModel) {
        $dataReport = [];
        foreach ($listIdUser as $key => $value) {
            $report = $reportModel->getAllDataReportOT($month['lastMonth'],$month['ThisMonth'],$value->id);
            if(count($report) != 0) {
                $dataReport[] = $report;
            }
        }
        return $dataReport;
    }
    /**
     *Export Excel file Over time
     */
    public function exportExcel( $month, UserRepository $userModel , ReportRepository $reportModel, ProjectRepository $projectModel, SettingRepository $settingModel) {
        $vt = strpos($month, '&');
        if($vt == 1) {
            $month = $str = substr( $month, 0, 1 );
        } else {
            $month = $str = substr( $month, 0, 2 );
        }

        try {
            $users = $this->getDataForFileExcel( $month, $userModel , $reportModel, $projectModel, $settingModel );
        }
        catch (Exception $e) {
            $mess = $e->getMessage();
            return Response::json(compact('mess'));
        }

        Excel::create('ExcelExport', function ($excel) use ( $users ){
            foreach ($users as $key => $value) {
                $this->createSheet ($excel, $value, $key);
            }

        // and Sheet
        })->export('xlsx');
    }
    private function createSheet ($excel, $users, $key) {
       return $excel->sheet($key, function ($sheet) use ($users, $key) {
// Format data for count time
            $sheet->setFontFamily('Times New Roman');
            $ngayLe = 0;
            $ngayThuong = 0;
            $ngayNghi = 0;
            $tongSoGio = 0;

            foreach ($users as $value) {
                if($value['Ngày lễ'] != "") {
                    $ngayLe = $ngayLe + $value['Số giờ'];
                } else if ($value['Ngày nghỉ'] != "" ){
                    $ngayNghi = $ngayNghi + $value['Số giờ'];
                } else {
                    $ngayThuong = $ngayThuong + $value['Số giờ'];
                }
                $tongSoGio = $tongSoGio + $value['Số giờ'];
            }

// first row styling and writing content AND common
            $sheet->mergeCells('A1:H1');
            $sheet->mergeCells('A2:F2');
            $sheet->mergeCells('G2:H2');
            $sheet->setWidth('A', 10);
            $sheet->setWidth('B', 15);
            $sheet->setWidth('C', 15);
            $sheet->setWidth('D', 15);
            $sheet->setWidth('E', 50);
            $sheet->setWidth('F', 25);
            $sheet->setWidth('G', 10);
            $sheet->setWidth('H', 10);
            $sheet->setWidth('i', 10);
            $sheet->setWidth('j', 10);
            $sheet->setFontSize(12);
            $sheet->row(1, function ($row) {
// $row->setFontFamily('Times New Roman');
                $row->setFontSize(18);
                $row->setFontWeight('bold');
                $row->setAlignment('center');
            });
            $sheet->setHeight(1, 100);
            $sheet->row(1, array('BẢN ĐĂNG KÝ LÀM THÊM NGOÀI GIỜ'));

            // second row styling and writing content
            $sheet->setHeight(2, 30);
            $sheet->row(2, function ($row) {
// call cell manipulation methods
// $row->setFontFamily('Times New Roman');
                $row->setFontSize(12);
                $row->setFontWeight('bold');

            });

            $sheet->row(2, array('✦ Người đăng ký: '.$key.'','','','','','','✦ Mã nhân viên:'));
            $sheet->appendRow(3, array(''));

// setting column names for data - you can of course set it manually
            $sheet->appendRow(array_keys($users[0])); // column names
// getting last row number (the one we already filled and setting it to bold
            $sheet->row($sheet->getHighestRow(), function ($row) {
                $row->setFontWeight('bold');
            });

// putting users data as next rows
            foreach ($users as $user) {
                $sheet->appendRow($user);
            }

// set height for content

            for($i = 4; $i <= (count($users) + 5); $i++) {
                $sheet->setHeight($i, 30);
            }

/*
 * setAlignment and setValignment
 */
            $d = 'F'.(count($users) + 5).':J'.(count($users) + 5);
            $sheet->setBorder($d, 'thin');
            $sheet->cell($d, function($cells) {
                $cells->setAlignment('center');
                $cells->setValignment('center');
                $cells->setFontWeight('bold');
                $cells->setFontSize(14);
            });
            $sheet->cells('D5:D'.(count($users) + 4), function($cells) {
                $cells->setAlignment('center');
                $cells->setValignment('center');
            });
            $sheet->cell('E4', function($cell) {
                $cell->setAlignment('center');
                $cell->setValignment('center');
            });
            $sheet->cell('E'.(count($users) + 8), function($cell) {
                $cell->setAlignment('right');
                $cell->setValignment('center');
            });
            $sheet->cells('E5:E'.(count($users) + 4), function($cells) {
                $cells->setAlignment('left');
                $cells->setValignment('center');
            });
            $sheet->cell('C4', function($cell) {
                $cell->setAlignment('center');
                $cell->setValignment('center');
            });
            $sheet->cells('C4:C'.(count($users) + 4), function($cells) {
                $cells->setAlignment('center');
                $cells->setValignment('center');
            });
            $sheet->cell('D4', function($cell) {
                $cell->setAlignment('center');
                $cell->setValignment('center');
            });
            $sheet->cells('B4:B'.(count($users) + 4), function($cells) {
                $cells->setAlignment('center');
                $cells->setValignment('center');
            });

            $sheet->cells('A4:A'.(count($users) + 4), function($cells) {
                $cells->setAlignment('center');
                 $cells->setValignment('center');
            });
            $sheet->cell('F4', function($cell) {
                $cell->setAlignment('center');
                $cell->setValignment('center');
            });
            $sheet->cells('F5:F'.(count($users) + 4), function($cells) {
                $cells->setAlignment('left');
                $cells->setValignment('center');
            });
            $sheet->cells('G4:G'.(count($users) + 4), function($cells) {
                $cells->setAlignment('center');
                $cells->setValignment('center');
            });
            $sheet->cells('H4:H'.(count($users) + 4), function($cells) {
                $cells->setAlignment('center');
                $cells->setValignment('center');
            });

            $sheet->cells('I4:I'.(count($users) + 4), function($cells) {
                $cells->setAlignment('center');
                $cells->setValignment('center');
            });
            $sheet->cells('J4:J'.(count($users) + 4), function($cells) {
                $cells->setAlignment('center');
                $cells->setValignment('center');
            });
/*
 * End setAlignment and setValignment
 */
            $setborderRow = 'A4:J'.(count($users) + 4);
            $sheet->setBorder($setborderRow, 'thin');
            // $sheet->getStyle('A1:K11')->getAlignment()->setWrapText(true);

            $merge = 'A'.(count($users) + 6).':'.'B'.(count($users)+ 6);
            $sheet->mergeCells($merge);
// Total h
            $sheet->row(count($users) + 5, array('','','','','','Tổng',$tongSoGio,$ngayThuong,$ngayNghi,$ngayLe));
// footer file
            $sheet->row(count($users) + 6, array('✦ Phần Xác nhận'));
            $cellBold   = 'A'.(count($users) + 6).':'.'A'.(count($users)+ 9);
// create avaliable merge
            $mergeTN    = 'A'.(count($users) + 8).':'.'B'.(count($users)+ 8);
            $mergeQL    = 'D'.(count($users) + 8).':'.'E'.(count($users)+ 8);
            $mergeQLGLT = 'G'.(count($users) + 8).':'.'H'.(count($users)+ 8);
            $sheet->mergeCells($mergeTN );
            // $sheet->mergeCells($mergeQL);
            $sheet->mergeCells($mergeQLGLT);

            $sheet->row(count($users) + 8, array('    Người đăng ký','','','Trưởng nhóm','Quản lý dự án',' ',' ',' ','Người quản lý giờ làm'));
            $sheet->cells($cellBold, function($cells) {
                $cells->setFontWeight('bold');
            });

            $sheet->row(count($users) + 8, function($row) {
                $row->setFontWeight('bold');
            });

        });
        $status= TMS_STATUS;
        return Response::json(compact('status'));
    }
}
