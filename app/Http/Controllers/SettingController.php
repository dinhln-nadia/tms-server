<?php
namespace TMSApp\Http\Controllers;

use TMSApp\Http\Controllers\Controller;
use TMSApp\Repositories\SettingRepository;
use Response;
use Input;
use Validator;
use Cache;
use HttpResponse;

class SettingController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
        $this->middleware('jwt.refresh');
    }
    

    /**
     * Api function for Install dayoff.
     *
     * @param input            
     * @return Response
     */
    public function installDayoff(SettingRepository $model) {
        $data = Input::all();
        $status = TMS_STATUS;
        $validator = Validator::make($data, [
            
            'day'           => 'required|date',
            'note'          => 'required',

        ]);

        // Thorw message error if fail.
        if ($validator->fails()) {

            return Response::json(['error' => $validator->messages()], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        // check issert row  setting dayofff
        $checkIssertDayoffRow = $model->checkIssertDayoff();
            $dataInput[] = $data;
        if( $checkIssertDayoffRow == false ) {
            $data = $this->installDayoffSetting ( $model, $dataInput, 'create');

            return Response::json(compact('status', 'data'));
         }

         $dataDayOff = unserialize($checkIssertDayoffRow->setting_value);
         
         foreach ($dataDayOff as $value) {
             if ( $value['day'] ==  $data['day']) {
                return Response::json(['error' => 'Day is duplicate'], HttpResponse::HTTP_NOT_ACCEPTABLE);
             }
         }         

         $dataDayOff[] = $data;
         $this->installDayoffSetting ( $model, $dataDayOff, 'update', $checkIssertDayoffRow->id );

         
        return Response::json(compact('status', 'dataDayOff'));
    }

    /**
     * Install setting dayoff 
     * @param  Array
     * @return String
     */
    private function installDayoffSetting ($model, $dataValue, $status, $id = null) {

        $value = serialize($dataValue);
        $dataInstall = [ 'setting_name' => 'dayoff', 'setting_value' => $value ];
        if ( $status == 'create' ) {
            $data = $model->InstallDayOff($dataInstall);
        } else {
            $data = $model->updateDayoff($dataInstall, $id);
        }

        return $data;
    }

    /**
     * Delete Custom dayoff
     */
    public function DeleteDayOff ( SettingRepository $model) {

        $day = Input::get('day');

        // Get data setting_value

        $data = $model->checkIssertDayoff();
        $dayoff = unserialize($data->setting_value);
        $key = array_search($day, array_column($dayoff, 'day'));
        unset($dayoff[$key]);
        $this->installDayoffSetting ( $model, $dayoff, 'update', $data->id );

        $status = TMS_STATUS;

        return Response::json(compact('status', 'dayoff'));
    }


    /**
     * Get List DayOff
     */
    public function getListDayOffSetting ( SettingRepository $model) {
        $status = TMS_STATUS;
        $data = $model->getlist();
        if (count($data) == 0) {
            $getData = [];
            return Response::json(compact('status', 'getData'));
        }
        $getData = unserialize($data->setting_value);
        return Response::json(compact('status', 'getData'));
    }


    /*
        message for top header
    */
    public function updateMessage( SettingRepository $model) {
        $data = Input::all();

        $check = $model->checkIssertMessage();

        if($check == false) {
            $model->create(array('setting_name' => 'Message','setting_value' => $data['mes']));
        }
            $model->update( array ( 'setting_value' => $data['mes'] ), 'Message', $attribute="setting_name");

        $data = array( 'message'=> $data['mes']);
        
        Cache::forever('keyMessage', $data['message']);

        return Response::json(compact('data'));
    }

    public function getMessage( SettingRepository $model ) {
        // $data = $model->getMessageForTop();
        $data = '';
        if(Cache::has('keyMessage')) {
            $data = Cache::get('keyMessage');
        }

        return Response::json(compact('data'));

    }

}   