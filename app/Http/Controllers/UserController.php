<?php
namespace TMSApp\Http\Controllers;

use TMSApp\Http\Requests;
use TMSApp\Http\Requests\UserRequest;
use TMSApp\Http\Requests\ForgotPassword;
use TMSApp\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use Input;
use JWTAuth;
use TMSApp\Models\User;
use Response;
use HttpResponse;
use Illuminate\Http\Exception\HttpResponseException;
use TMSApp\Repositories\UserRepository;
use TMSApp\Repositories\MissingReportRepository;
use TMSApp\Repositories\ReportRepository;
use TMSApp\Repositories\UserRoleRepository;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except'=>['login', 'forgotPassword', 'sendEmailForgotInforUser']]);
        $this->middleware('jwt.refresh', ['except'=>['login', 'forgotPassword', 'sendEmailForgotInforUser']]);
        $this->middleware('admin', ['only'=>['store', 'changePasswordAdmin']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(UserRepository $model)
    {
        $result = $model->all();
        $data   = $result->all();
        $status = TMS_STATUS;
        return Response::json(compact('status', 'data') );
    }

    /**
     * Display all users in database.
     *
     * @return Response
     */
    public function getAllName(UserRepository $model)
    {
        $result = $model->getAllName();
        $data = $result->all();
        $status = TMS_STATUS;
        return Response::json(compact('status', 'data') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * API function for user login
     *
     * @return Response
     */
    public function login()
    {
        $data = Input::all();

        // Create validator
        $validator = Validator::make($data, [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6'
        ]);

        // Thorw message error if fail.
        if ($validator->fails()) {
            return Response::json([
                'error' => $validator->messages()
            ], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        if (! $token = JWTAuth::attempt($data, [
            'test' => '123'
        ])) {
            return Response::json(false, HttpResponse::HTTP_UNAUTHORIZED);
        }

        $user = JWTAuth::toUser($token);
        if ($user['status'] == 0) {
            return Response::json([
                'error' => trans('message.account_is_block')
            ], HttpResponse::HTTP_ACCEPTED);
        }
        $payload = JWTAuth::getPayload($token);
        if($user->isAdmin()){
          $premission = 1;
        }else{
          $premission = 2;
        }

        return Response::json(compact('token', 'user', 'payload', 'premission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(UserRepository $model, MissingReportRepository $model_missing, UserRoleRepository $user_role)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $mytime = Carbon::now();
        $data = Input::all();
        $dataInsert= [];

        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6'
        ]);

        if ($validator->fails()) {
            return Response::json([
                'error' => $validator->messages()
            ], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        if($user->isAdmin()){
            try {
                $data['password'] = bcrypt($data['password']);
                $data['status'] = 1;
                $user = $model->create($data);
            } catch (\Exception $e) {
                return Response::json([
                    'error' => trans('message.user_exists')
                ], HttpResponse::HTTP_CONFLICT);
            }
             //insert table user_role
            $user_role->createUserRole( [ 'role_id' => '2', 'user_id' => $user->id ] );

            // set dayoff before register day
            if(Carbon::parse($mytime)->format('d') > 1){
              for($i=1; $i < Carbon::parse($mytime)->format('d') ; $i++){
                $dataInsert["day_$i"] = TMS_DAYOFF;
              }
            }
            $dataInsert['user_id'] = $user->id;
            $dataInsert['month']    = Carbon::parse($mytime)->format('m');
            $dataInsert['year']     = Carbon::parse($mytime)->format('Y');

            //insert table missing_report
            $model_missing->createMissingRow($dataInsert);

            // Generate new jwt token.
            // $token = JWTAuth::fromUser($user, [
            //     'random_id' => 'test'
            // ]);

            $status = TMS_STATUS;

            return Response::json(compact('status'));
        }else{
            return Response::json(['error' => trans('message.permission')], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id, UserRepository $model)
    {
        $data = $model->getDetailUser($id);
        $status = TMS_STATUS;
        return Response::json(compact('status', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(UserRepository $model, $id)
    {
        $data = $model->find($id, array(
            "*"
        ));
        return Response::json(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update($id, UserRepository $model)
    {

        $data = Input::all();
        $user = JWTAuth::parseToken()->authenticate();
        $datas = array(
            'email' => $data['email'],
            'name' => $data['name']
        );

        // Create validator
        $validator = Validator::make($datas, [
            'email' => 'required|email|max:255',
            'name' => 'required|max:255'
        ]);

        // Thorw message error if fail.
        if ($validator->fails()) {
            return Response::json([
                'error' => $validator->messages()
            ], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        $model->update($datas, $id, $attribute = 'id');

        return Response::json(array(
            'messages' => 'success'
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id, UserRepository $model)
    {
        $user = JWTAuth::parseToken()->authenticate();
        try {
            $result = $model->delete($id);
        } catch (Exception $e) {
            return Response::json(['error' => trans('message.update_error')], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        return Response::json(array('status'=>'success'));

    }

    /**
     * Forgot password
     *
     * @param ForgotPassword $request
     * @param UserRepository $model
     * @return Response
     */
    public function forgotPassword(UserRepository $model)
    {
        $data = Input::all();

        // Create validator
        $validator = Validator::make($data, [
            'email' => 'required|email|max:255',
            'question_id' => 'required|numeric',
            'answer' => 'required'
        ]);

        // Thorw message error if fail.
        if ($validator->fails()) {
            return Response::json([
                'error' => $validator->messages()
            ], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        extract($data);

        $record = User::whereRaw('email = ? AND question_id = ? AND answer = ?', array(
            $email,
            $question_id,
            $answer
        ))->first();

        if (! $record) {
            return Response::json([
                'error' => trans('message.answer_invalid')
            ], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        $password = substr(md5(rand()), 0, 10);

        // Update and return password
        $datas['password'] = bcrypt($password);
        $model->update($datas, $record->id, $attribute = 'id');

        return Response::json(compact('password'));
    }

    /**
     * Change password function.
     *
     * @param UserRepository $model
     * @return Response
     */
    public function changePassword(UserRepository $model)
    {
        $data = Input::all();
        $user = JWTAuth::parseToken()->authenticate();

        // Create validator
        $validator = Validator::make($data, [
            'old_password' => 'required|min:6',
            'password' => 'required|min:6'
        ]);

        $checkOldPassword = [
            'email' => $data['email'],
            'password' => $data['old_password']
        ];

        // Check password
        if (! $checkData = JWTAuth::attempt($checkOldPassword)) {
            return Response::json([
                'error' => 'Password is incorrect !'
            ], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        // Update new password
        $password = bcrypt($data['password']);
        $user = $model->update([
            'password' => $password
        ], $data['email'], $attribute = "email");

        return Response::json([
            'message' => trans('message.change_password')
        ], HttpResponse::HTTP_OK);
    }

    /**
     * @param array
     *
     *@return Response
     */
    public function changePasswordAdmin(UserRepository $model)
    {
       $data = Input::all();
       $user = JWTAuth::parseToken()->authenticate();
       // Create validator
        $validator = Validator::make($data, [
            'password' => 'required|min:6'
        ]);

        // Update new password
        $password = bcrypt($data['password']);
        $user = $model->update( ['password' => $password ], $data['id'], $attribute = "id");

        return Response::json([
            'message' => trans('message.change_password')
        ], HttpResponse::HTTP_OK);

    }

    /**
     * Logout function
     *
     * @return void
     */
    public function logout()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        JWTAuth::refresh($token);
    }

    /**
     * Search user function.
     *
     * @param UserRepository $model
     * @return Response
     */
    public function search(UserRepository $model)
    {
        $data = Input::except('token');

        $validator = Validator::make($data, [
            'name' => 'max:255',
            'email' => 'max:255'
        ]);

        // If validation fails return error message.
        if ($validator->fails()) {

            return Response::json([
                'error' => $validator->messages()
            ], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }

        $pattern = [];
        $pattern['name'] = Input::get('name') . '%';
        $pattern['email'] = Input::get('email') . '%';
        $project = $model->search($pattern);

        return Response::json(compact('project'));

    }

    /**
     * Get a listing disable of the resource.
     *
     * @return Response
     */
    public function listDisable(UserRepository $model)
    {
        $result = $model->listDisable(['id', 'name', 'email']);
        $data = $result->all();
        $status = TMS_STATUS;

        return Response::json(compact('status', 'data'));
    }

     /**
     * Get a listing disable of the resource.
     *
     * @return Response
     */
    public function enableListId(UserRepository $model)
    {
         $data = Input::except('token');

        $validator = Validator::make($data, [
            'users'   => 'required|string',
        ]);
        // If validation fails return error message.
        if ($validator->fails()) {

            return Response::json([
                'error' => $validator->messages()
            ], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        // Convert string users_id to array.
        $integerIDs = array_map('intval', explode(',', $data['users']));
        // Restone user in list id
        $result = $model->enable($integerIDs);

        if($result >= 1){
            return Response::json(array('status'=>'success'));
        }
        return Response::json(['error' => trans('message.update_error')], HttpResponse::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function missingReport(MissingReportRepository $report_missing, ReportRepository $report)
    {
        $data = Input::all();
        $user = JWTAuth::parseToken()->authenticate();
        // Create validator
        $validator = Validator::make($data, [
            'month' => 'integer',
            'year' => 'integer',
        ]);

        // Thorw message error if fail.
        if ($validator->fails()) {
            return Response::json(['error' => $validator->messages()], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        $month  = isset($data['month']) ?  $data['month'] : 0;
        $year   = isset($data['year']) ?  $data['year'] : 0;
        $now = Carbon::now();

        $result = $report_missing->missingReport($month, $year, $user);
        $data = [];

        if( $result ){
            if( $month == 0 || $month == $now->month ){
                $day = $now->day -1;
            }else {
                $now = Carbon::createFromDate($result->year, $result->month,1);
                $day = $now->daysInMonth;
            }
            for ($i=1; $i <= $day; $i++) {
                $isWeekend = Carbon::createFromDate($result->year, $result->month, $i)->isWeekend();
                $thisDayColumn = 'day_'.$i;
                if( !$isWeekend && $result[$thisDayColumn] == TMS_MISSING){
                    $tmp = $report->reportInDay($user->id, $i, $now)->count();
                    if($tmp==0){
                        $status = TMS_CAN_DAYOFF;
                    }else{
                        $status = TMS_NOT_DAYOFF;
                    }

                    $data[] = ['day' =>$i, 'status' => $status];
                }
            }
        }

        $status= TMS_STATUS;
        return Response::json(compact('status', 'data'));
    }

    public function sendEmailForgotInforUser()
    {
        $data = Input::all('email');
        $validator = Validator::make($data, [
            'email' => 'email | required'
        ]);

        if ($validator->fails()) {
          return Response::json(['error' => $validator->messages()], HttpResponse::HTTP_NOT_ACCEPTABLE);
        }
        $check = User::where('email', $data['email'])->first();
        if( $check == null )
          return Response::json(['error' => 'Sorry but that email address is not in our records, please try again.'], HttpResponse::HTTP_NOT_ACCEPTABLE);

        // Create random password 10 charater
        $length = 10;
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
        $password   = bcrypt($randomString);

        // Save password
        User::where('email',$data['email'])->update(['password' => $password]);
        // Send email to user
        $email = $data['email'];
        try {

          Mail::send('emailPassword', [ 'email' => $data['email'], 'string' => $randomString ], function ($message) use ($email) {
              $message->from(env('MAIL_USERNAME'), '[TMS] Change Password');
              $message->to($email)->subject('[TMS] Change Password ');
          });

        } catch (Exception $e) {
            return $e->getMessage();
        }

        $status= TMS_STATUS;

        return Response::json(compact('status'));

    }
}
