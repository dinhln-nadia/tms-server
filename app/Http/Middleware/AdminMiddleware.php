<?php namespace TMSApp\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Middleware\BaseMiddleware;

class AdminMiddleware extends BaseMiddleware{

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$token = $this->auth->setRequest($request)->getToken();
        if( !$this->auth->setRequest($request)->authenticate($token)->isAdmin() ){
            return $this->respond('', 'Are you Admin ?', 404);
        }
		return $next($request);
	}

}
