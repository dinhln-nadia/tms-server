<?php namespace TMSApp\Http\Requests;
use HttpResponse;
use Response;
use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest {

	public function response(array $errors)
    {
        if ($this->ajax() || $this->wantsJson())
        {
            return new JsonResponse($errors, 422);
        }
        return Response::json(['error' => $errors],HttpResponse::HTTP_NOT_ACCEPTABLE);

    }

}
