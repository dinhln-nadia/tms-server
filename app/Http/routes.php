<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(array('prefix' => 'api/v1'), function () {
    // Dashboard
    Route::get('/projects/my-dashboard', 'ProjectController@myDashboard');
    Route::get('/projects/company-dashboard', 'ProjectController@companyDashboard');
    Route::get('/projects/project-dashboard', 'ProjectController@projectDashboard');

    /* For users endpoint */
    Route::get('/users/logout', 'UserController@logout');
    Route::get('/users/search', 'UserController@search');
    Route::get('/users/list-disable', 'UserController@listDisable');
    Route::get('/users/missing-report', 'UserController@missingReport');
    Route::get('/users/id-name', 'UserController@getAllName');
    Route::post('/users/login', 'UserController@login');
    Route::post('/users/forgot-password', 'UserController@forgotPassword');
    Route::post('/users/change-password', 'UserController@changePassword');
    Route::post('/users/change-password-admin', 'UserController@changePasswordAdmin');
    Route::post('/users/enable', 'UserController@enableListId');
    Route::post('/users/sendEmail', 'UserController@sendEmailForgotInforUser');

    Route::resource('users', 'UserController',
        ['only' => ['index', 'store','edit', 'show', 'update', 'destroy']]);

    /* For settings endpoint */
    Route::post('/settings/install-dayoff', 'SettingController@installDayoff');
    Route::post('/settings/delete-dayoff', 'SettingController@DeleteDayOff');
    Route::get('/settings/list-dayoff', 'SettingController@getListDayOffSetting');
    Route::post('/settings/update-message', 'SettingController@updateMessage');
    Route::get('/settings/getMessage', 'SettingController@getMessage');

    /* For categories endpoint */
    Route::resource('categories', 'CategoryController',
        ['only' => ['index', 'store','update', 'destroy']]);

    /* For project endpoint */
    Route::post('/projects/{id}/add-user', 'ProjectController@addUser');
    Route::post('/projects/{id}/remove-user', 'ProjectController@removeUser');
    Route::post('/projects/projects-user-quotation', 'ProjectController@ProjectUserRepository');
    Route::get('/projects/getFreeTime/{project_id}', 'ProjectController@getFreeTime');
    Route::get('/projects/{project_id}/users_not_in', 'ProjectController@getUserNotInProject');
    Route::get('/projects/search', 'ProjectController@search');
    Route::get('/projects/list-disable', 'ProjectController@listDisable');
    Route::post('/projects/{id}/toggle_status', 'ProjectController@enableId');
    Route::get('/projects/id-name', 'ProjectController@getAllNameOfProject');
    Route::resource('projects', 'ProjectController',
        ['only' => ['index', 'store', 'update', 'show', 'destroy']]);

    /* For report endpoint */
    Route::get('/reports/search', 'ReportController@search');
    Route::get('/reports/missing', 'ReportController@missingReport');
    Route::post('/reports/missing/day_off', 'ReportController@updateMissingReport');
    Route::get('/reports/duplicate-day', 'ReportController@duplicateDay');
    // Route::get('/reports/overtime', [ 'as'  => 'report.overtime', 'uses'   => 'ReportController@overtimeReport']);

    // For overtime and absence
    Route::post('/reports/overtime/{id}/accept', ['as'  =>  'reports.overtime.accept', 'uses'  =>  'ReportController@acceptOvertime']);
    Route::post('/reports/overtime/{id}/reject', ['as'  =>  'reports.overtime.reject', 'uses'  =>  'ReportController@rejectOvertime']);
    Route::post('/reports/absence/{id}/accept', ['as'  =>  'reports.absence.accept', 'uses'  =>  'ReportController@acceptAbsence']);
    Route::post('/reports/absence/{id}/reject', ['as'  =>  'reports.absence.reject', 'uses'  =>  'ReportController@rejectAbsence']);

    Route::resource('reports', 'ReportController',
        ['only' => ['index', 'store', 'update', 'destroy']]);

});
Route::get('avdtntg/export-excel/{month}', 'ReportController@exportExcel');