<?php namespace TMSApp\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectUser extends Model {

    // use SoftDeletes;
    public $timestamps = true;
	/**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'project_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['project_id', 'user_id'];
    // protected $dates = ['deleted_at'];

}
