<?php namespace TMSApp\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Projects extends Model {
    
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'quotation_time', 'plan_time'];
    protected $dates = ['deleted_at'];
    /**
     * 
     * @return response
     */
    public function users()
    {
        return $this->belongsToMany('TMSApp\Models\User', 'project_user', 'project_id');
    }

    /**
     * 
     * @return response
     */
    public function reports()
    {
        return $this->hasMany('TMSApp\Models\Reports','project_id')->select('project_id');
    }
}
