<?php namespace TMSApp\Models;

use Illuminate\Database\Eloquent\Model;

class ReportCategory extends Model {

	public $timestamps = true;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'report_category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['report_id', 'category_id'];

}
