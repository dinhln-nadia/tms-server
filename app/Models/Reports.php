<?php namespace TMSApp\Models;

use Illuminate\Database\Eloquent\Model;

class Reports extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reports';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'project_id','ticket', 'user_id', 'note', 'cost', 'start_time','end_time', 'type'];

    /**
     * Create relationship category.
     * 
     * @return response
     */
    public function category()
    {
        return $this->belongsToMany('TMSApp\Models\Categories', 'report_category', 'report_id', 'category_id')
                    ->select('categories.name', 'categories.type', 'categories.id')->where('type', TMS_CATEGORY);
    }

    /**
     * Create relationship process.
     * 
     * @return response
     */
    public function process()
    {
        return $this->belongsToMany('TMSApp\Models\Categories', 'report_category', 'report_id', 'category_id')
                    ->select('categories.name', 'categories.type', 'categories.id')->where('type', TMS_PROCESS);
    }
}
