<?php namespace TMSApp\Repositories;
 
use TMSApp\Repositories\Contracts\RepositoryInterface;
use TMSApp\Repositories\Eloquent\Repository;

class CategoryRepository extends Repository {


    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'TMSApp\Models\Categories';
    }

    /**
     * @param int $type
     * @param array $columns
     * @return Response
     */
    public function getCategories($type, $columns = array('*'))
    {
    	return $this->model->where('type', '=', $type)->whereNotIn('id',[1,2])->get($columns);
    }

    /**
     * get all category 
     */
    public function getAllCategory()
    {
        return $this->model
                    ->join('report_category', 'categories.id', '=', 'report_category.category_id')
                    ->select('categories.name','categories.type','report_category.report_id')
                    ->whereNotIn('categories.id',[1,2])
                    ->get();
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function getData($attribute, $value, $columns = array('*')) {
        return $this->model->where($attribute, '=', $value)->get($columns);
    }

    /**
     * 
     * @return All category
     */
    public function getwithTrashed($columns = array('*')) {
        return $this->model->get($columns);
    }

    public function getOnlyWithTrashed($type)
    {
        return $this->model->select('id')->onlyTrashed()->where('type', '=', $type)->get();
    }

}