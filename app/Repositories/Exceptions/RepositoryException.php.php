<?php namespace TMSApp\Repo\Eloquent\Exceptions;

/**
 * Class RepositoryException
 * @package Bosnadev\Repositories\Exceptions
 */
class RepositoryException extends \Exception {}