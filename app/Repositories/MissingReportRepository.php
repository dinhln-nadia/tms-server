<?php namespace TMSApp\Repositories;
 
use TMSApp\Repositories\Contracts\RepositoryInterface;
use TMSApp\Repositories\Eloquent\Repository;
use Carbon\Carbon;

class MissingReportRepository extends Repository {


    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'TMSApp\Models\MissingReports';
    }

    /**
     * Return days missing report of user
     * @return mixed
     */
    public function missingReport($month, $year, $user = null, $listUserDelete = null)
    {
        if ( $month == 0){ $month = Carbon::now()->month; }
        if ( $year == 0){ $year = Carbon::now()->year; }

        if($user) {
            return  $this->model->where('user_id', '=', $user->id)
                                ->where('month', '=', $month)
                                ->where('year', '=', $year)
                                ->first();
        }
        if($listUserDelete) {
            return  $this->model->where('month', '=', $month)
                            ->where('year', '=', $year)->whereNotIn('user_id', $listUserDelete)->get();
        }
        return  $this->model->where('month', '=', $month)
                            ->where('year', '=', $year)->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id) 
    {
        return $this->model->where('report_id', '=', $id)->delete();
    }

    /**
     * @param array $data
     * @param array $pattern
     * @return mixed
     */
    public function update(array $data, $pattern) {
        return $this->model->where($pattern, '=')->update($data);
    }

    /**
     * @param  array $pattern 
     * @return mixed
     */
    public function getByPatter($pattern)
    {
        return $this->model->where($pattern, '=')->get();
    }

    /**
    *
    * @param array
    * @return mixed
    */
    public function createMissingRow($data)
    {
        return $this->model->insert($data);
    }


    /**
     * @param user_id, month, day, year,
     * @return mixed
     */
    public function updateMissing($user_id, $year, $month, $day) {
        return $this->model->where('month', '=', $month)
                    ->where('year', '=', $year)
                    ->where('user_id', '=', $user_id)
                    ->update(array($day => TMS_REPORT_DONE));
    }

    /**
     * @param user_id, month, day, year,
     * @return mixed
     */
    public function updateMissingNG($user_id, $year, $month, $day) {
        return $this->model->where('month', '=', $month)
                    ->where('year', '=', $year)
                    ->where('user_id', '=', $user_id)
                    ->update(array($day => TMS_NOT_DAYOFF));
    }
}