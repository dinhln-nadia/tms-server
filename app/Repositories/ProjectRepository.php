<?php namespace TMSApp\Repositories;
 
use TMSApp\Repositories\Contracts\RepositoryInterface;
use TMSApp\Repositories\Eloquent\Repository;
use Carbon\Carbon;

class ProjectRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'TMSApp\Models\Projects';
    }

    /**
     * @param string $pattern
     * @param array $columns
     * @return mixed
     */
    public function search($pattern, $option = fails , $columns = array('*')) 
    {
        if( $option ){
            return $this->model->where('name', 'LIKE', $pattern)->onlyTrashed()->paginate(TMS_PERPAGE,$columns);
        }
        return $this->model->where('name', 'LIKE', $pattern)->paginate(TMS_PERPAGE,$columns);
    }

    /**
     * @param  int $id 
     * @return mixed
     */
    public function getProjectDetail($id)
    {
       return $this->model->with([ 'users' => function($q){
                        $q->select('users.id','users.name', 'users.email','project_user.member_quotation');
                    }])
                    ->where('projects.id','=',$id)
                    ->get();
    }

    /**
     * @param int $perPage
     * @return mixed
     */
    public function getDeltail( $user) 
    {
        if ( $user->isAdmin() ) {
            // return $this->model
            //             ->selectRaw('projects.id, projects.name, count(project_user.user_id) as member')
            //             ->leftJoin('project_user', 'projects.id', '=', 'project_user.project_id')
            //             ->groupBy('projects.id')->paginate(TMS_PERPAGE);
            return $this->model
                        ->selectRaw('projects.id, projects.name, count(project_user.user_id) as member')
                        ->leftJoin('project_user', 'projects.id', '=', 'project_user.project_id')
                        ->groupBy('projects.id')->whereNotIn('projects.id',[1])->get();
        }
        return $this->model
                    ->selectRaw('projects.id, projects.name, count(project_user.user_id) as member, projects.quotation_time, projects.plan_time')
                    ->leftJoin('project_user', 'projects.id', '=', 'project_user.project_id')
                    ->where('project_user.user_id', '=', $user->id)
                    ->groupBy('projects.id')->whereNotIn('projects.id',[1])->get();
    }


    public function companyDashBoard()
    {
        $now = Carbon::now();
        $start_time = $now->year."-".$now->month."-".$now->day." 00:00:00";
        $end_time = $now->year."-".$now->month."-".$now->day." 23:59:00";
        return $this->model->select()->with('reports')->get();
        // return $this->model->select('projects.name', 'projects.quotation_time', 'projects.plan_time', \DB::raw('count(reports.id) as reports'))
        //             ->join('reports', 'projects.id', '=', 'reports.project_id')
        //             // ->where('start_time', '=', $start_time)
        //             // ->where('end_time', '=', $end_time)
        //             ->get();
    }

    /**
     * @param number
     * @return number
     */
    public function checkQuotationTime($project_id)
    {
        $time_project = $this->model->select('quotation_time')->where('id', $project_id)->first();
        if($time_project->quotation_time == null)
            return 0;
        return $time_project->quotation_time;
    }


    public function getAllProjecct()
    {
        return $this->model
            ->select('name as label', 'quotation_time as project_quotation','projects.id as project_id')
            ->whereNotIn('projects.id', [1])
            ->get();
    }

    public function getNameProject($id) {
        $data = $this->model->find($id);
        return $data->name;
    }
}
