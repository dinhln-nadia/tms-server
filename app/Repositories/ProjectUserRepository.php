<?php namespace TMSApp\Repositories;
 
use TMSApp\Repositories\Contracts\RepositoryInterface;
use TMSApp\Repositories\Eloquent\Repository;
use Carbon\Carbon;

class ProjectUserRepository extends Repository {


    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'TMSApp\Models\ProjectUser';
    }

    /**
     * @param int $type
     * @return Response
     */
    public function insertProject($id)
    {
        return $this->model->insert($id);
    }

    /**
     * @param int $project_id
     * @param array $users_id
     * @return Response
     */
    public function getUserId($project_id, $users_id)
    {
        return $this->model->select('user_id','project_id')
                    ->where('project_id', $project_id)
                    ->whereIn('user_id', $users_id)
                    ->get();
    }

    /**
     * Delete row where pattern 
     * @param int $project_id
     * @param array $users_id
     * @return Respone
     */
    public function removeUser( $project_id, $users_id)
    {
        return $this->model->whereIn('user_id', $users_id)
                           ->where('project_id', $project_id)
                           ->delete();
    }

    /*
     * @param int $project_id
     * @return Response
     */
    public function getUserInproject($project_id)
    {
        return $this->model->select('user_id')
                          ->where('project_id', $project_id)
                          ->get();
    }

    /*
     * @param int $project_id
     * @return Response
     */
    public function updateQuotaionTime($data, $project_id,$user_id)
    {
        return $this->model->where('project_id', $project_id)
                            ->where('user_id', $user_id)
                            ->update(['actual_work'=>$data]);
    }

    public function checkQuotationTimeUseForFreeTime($project_id)
    {
        $total = 0;
        $member_quotation = $this->model->select('member_quotation')
                                        ->where('project_id', $project_id)
                                        ->get();

        return $this->GetTimeFree($member_quotation) ;                               
    }
    /**
     *@param number
     *@return number
     */
    public function checkQuotationTimeUse($project_id, $user_id)
    {
        $user = array($user_id);
        $member_quotation =  $this->model->select('member_quotation')

                          ->where('project_id', $project_id)

                          ->whereNotIn('user_id', $user)

                          ->get();

        if($member_quotation == null)

            return $total = 0;

        return $this->GetTimeFree($member_quotation);
        
    }   

    private function GetTimeFree($member_quotation)
    {
        $total = 0;
        foreach ($member_quotation as $value) {
            if($value->member_quotation != null)

                $total = $total + $value->member_quotation;

        }
        return $total;
    }

    /**
     *@param int $project_id, int $user_id
     *@return Response
     */

    public function updateQuotation( $time, $project_id, $user_id )
    {
        $this->model->where('project_id', $project_id)
                    ->where('user_id',$user_id)
                    ->update(['member_quotation'=> $time]);
    }

    public function getListProject($user_id)
    {
        return $this->model
            ->join('projects', 'project_user.project_id', '=', 'projects.id')
            ->select('projects.name as label', 'project_user.member_quotation as project_quotation','project_user.actual_work','projects.id as project_id')
            ->where('project_user.user_id',$user_id)
            ->whereNotIn('projects.id', [1])
            ->get();
    }

    public function getDetailuserProject($project_id)
    {
        return $this->model
                    ->join('users', 'project_user.user_id', '=', 'users.id')
                    ->select('users.name', 'project_user.member_quotation as project_quotation', 'project_user.actual_work','users.id as user_id')
                    ->where('project_user.project_id', $project_id)
                    ->get();
    }

    public function getUserProject($project_id , $listDelete)
    {
        return $this->model
                    ->where('project_id', $project_id)
                    ->whereNotIn('user_id', $listDelete)
                    ->get();
    }
}
