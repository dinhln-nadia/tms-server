<?php namespace TMSApp\Repositories;

use TMSApp\Repositories\Contracts\RepositoryInterface;
use TMSApp\Repositories\Eloquent\Repository;
use TMSApp\Repositories\CategoryRepository;

class ReportCategoryRepository extends Repository {


    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'TMSApp\Models\ReportCategory';
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->where('report_id', '=', $id)->delete();
    }

    public function getListReportCategory($listCategories, $user_id, $startTime, $endTime)
    {
        return $this->model
            ->join('reports', 'reports.id', '=', 'report_category.report_id' )
            ->select(
                'reports.id as report_id',
                'reports.project_id as project_id',
                'reports.user_id as user_id',
                'report_category.category_id as category_id',
                'reports.ticket as ticket',
                'reports.cost as cost'
                )
            ->whereIn('report_category.category_id', $listCategories)
            ->whereIn('reports.type', [TMS_NORMAL_TYPE,TMS_OT_ACCEPT_TYPE] )
            ->where('reports.user_id', $user_id)
            ->where('reports.start_time', '>=', $startTime)
            ->where('reports.end_time', '<=', $endTime)
            ->get();
    }

    /**
     *@param   $id category
     *@return  Status
     */

    public function UpdateCategoryIdHasDelete($Category_id, $caregory, $develop)
    {
        $listCategories = [];
        foreach ($caregory as $value) {
           $listCategories[] = $value['id'];
        }

        $this->model->whereIn('category_id', $listCategories)->update(['category_id' => $develop]);
    }
}
