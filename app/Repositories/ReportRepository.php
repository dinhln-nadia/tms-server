<?php namespace TMSApp\Repositories;

use TMSApp\Repositories\Contracts\RepositoryInterface;
use TMSApp\Repositories\Eloquent\Repository;
use Carbon\Carbon;

class ReportRepository extends Repository {


    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'TMSApp\Models\Reports';
    }

    /**
     * Get list report
     *
     * @return mixed
     */
    public function getAll($user)
    {
        $thisDay = Carbon::now();
        $preDay = Carbon::createFromDate($thisDay->format('Y'), $thisDay->format('n'));

        if ($user->isAdmin()){
            return $this->model->with('category')->with('process')
                        ->where('start_time', '>=', $preDay)
                        ->where('end_time', '<=', $thisDay)
                        ->paginate(TMS_PERPAGE);
        }
        return $this->model->with('category')->with('process')
                    ->where('user_id', '=', $user->id)
                    ->where('start_time', '>=', $preDay)
                    ->where('end_time', '<=', $thisDay)
                    ->paginate(TMS_PERPAGE);
    }

    /**
     * Search in report
     *
     * @return mixed
     */
    public function search($user, $pattern, $time, $categories, $data, $type = null, $page = null)
    {
        $result = $this->model->with('category')->with('process')
                    ->whereHas('category', function($q) use ($categories){
                        if($categories['category'] <> null ){
                            $q->where('categories.id', '=', $categories['category'] );
                        }
                    })
                    ->whereHas('process', function($q) use ($categories){
                        if( $categories['process']<> null ){
                            $q->where('categories.id', '=', $categories['process'] );
                        }
                    })
                    ->where($pattern, '=')
                    ->where('start_time', '>=', $time['preDay'])
                    ->where('end_time', '<=', $time['thisDay']);

        if(isset($data['ticket']))
        {
            $result = $result->where('ticket', 'like', '%' . $data['ticket'] . '%');
        }

        if(isset($data['key_note']))
        {
            $result = $result->where('note', 'like', '%' . $data['key_note'] . '%');
        }

        if($type != null){

            $result = $result->whereIn('type', $type);
        }

        $result = $result->orderBy('start_time', 'desc');

        $count = isset($data['count']) ? $data['count'] : null;
        if ($count == 'All') {
            $count = TMS_SEARCH_ALL;
        }
        $per_page = TMS_SEARCH_ALL;
        if($page){
            // search with page, paginate with TMS_SEARCH_PERPAGE items
            $per_page = TMS_SEARCH_PERPAGE;
        }
        if($count && is_numeric($count)){
            // search with count, paginate with count number items
            $per_page = $count;
        }
        $result = $result->paginate($per_page)->appends($data);

        return $result;
    }

    /**
     *
     */
    public function reportInDay($user_id, $day, $now, $time = null)
    {
        if($time){
            $start_time = $now->year."-".$now->month."-".$day.$time['start'];
            $end_time = $now->year."-".$now->month."-".$day.$time['end'];
        }else{
            $start_time = $now->year."-".$now->month."-".$day." 00:00:00";
            $end_time = $now->year."-".$now->month."-".$day." 23:59:00";
        }

        return $this->model->select('cost', 'start_time', 'end_time')
                    ->where('user_id', '=', $user_id)
                    ->where('start_time', '>=', $start_time)
                    ->where('end_time', '<=', $end_time)
                    ->get();
    }

    public function findRerportByStarEnd($start_time, $end_time, $user_id)
    {
        return $this->model
                    ->where('start_time', '=', $start_time)
                    ->where('end_time', '=', $end_time)
                    ->where('user_id', '=', $user_id)
                    ->get();
    }

    public function getReportBetweenTime($start_time, $end_time)
    {
        return $this->model->select('cost', 'start_time', 'end_time','project_id','type')
            ->where('start_time', '>=', $start_time)
            ->where('end_time', '<=', $end_time)
            ->whereIn('type',[ TMS_NORMAL_TYPE, TMS_OT_ACCEPT_TYPE ])
            ->get();
    }

    public function getReports($project_id, $now)
    {
        return $this->model
                    ->where('project_id', $project_id)
                    ->where('start_time','>=', $now['start_time'])
                    ->where('end_time','<=', $now['end_time'])
                    ->whereIn('type',[ TMS_NORMAL_TYPE, TMS_OT_ACCEPT_TYPE ])
                    ->get();
    }

    /**
    *   Get list report of project .
    * @param int $project_id
    * @param array $now
    */
    public function reportsOfCategory($project_id, $now)
    {
        return $this->model->select('reports.cost', 'report_category.category_id', 'reports.project_id' )
                    ->join('report_category', 'report_category.report_id', '=', 'reports.id')
                    ->where('project_id', $project_id)
                    ->where('start_time','>=', $now['start_time'])
                    ->where('end_time','<=', $now['end_time'])
                    ->get();
    }

    /**
     * Get all report in day for duplicate report
     * @param start_time and end_time
     */
    public function getAllReportInDay ($date, $user_id) {
        return $this->model->with('category')->with('process')
                    ->where('start_time','>=', $date['start_time'])
                    ->where('end_time','<=', $date['end_time'])
                    ->where('reports.user_id', $user_id)
                    ->get();
    }

    /**
     * Get all report in day for duplicate report
     * @param start_time and end_time
     */
    public function getAllReportLastDay ($date, $user_id) {
        return $this->model->with('category')->with('process')
                    ->where('start_time','>=', $date['start_time'])
                    ->where('end_time','<=', $date['end_time'])
                    ->where('reports.type', TMS_NORMAL_TYPE)
                    ->where('reports.user_id', $user_id)
                    ->get();
    }


    /**
     * @param $time
     * @return boolean
     */

    public function checkCost( $time , $user_id) {
        $data = $this->model->where('start_time','>=', $time['start'])->where('end_time', '<=' ,$time['end'])->where('user_id', $user_id)->get();
        $cost = 0;
        if( count($data) == 0 ) {

           return false;

        }

        foreach ($data as $value) {
            $cost = $cost + $value->cost;
        }

        if( $cost >= 480 ) {

            return true;

        } else {

            return false;

        }
    }


    public function getAllDataReportOT($start_time, $end_time, $user_id)
    {
        return $this->model
                    ->select('project_id', 'note', 'cost','start_time','user_id','end_time')
                    ->where('start_time', '>=', $start_time)
                    ->where('end_time', '<=', $end_time)
                    ->where('type', '=', TMS_OT_ACCEPT_TYPE)
                    ->where('user_id', '=', $user_id)
                    ->orderBy('start_time', 'asc')
                    ->get()->toArray();
    }
}

