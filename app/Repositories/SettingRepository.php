<?php namespace TMSApp\Repositories;
 
use TMSApp\Repositories\Contracts\RepositoryInterface;
use TMSApp\Repositories\Eloquent\Repository;

class SettingRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'TMSApp\Models\Settings';
    }

    /**
     * Check row missing issert
     */

    public function checkIssertDayoff() {

        $check = $this->model->where('setting_name', 'dayoff')->first();


        if(count($check) == 0 ) {

            return false;

        }

        return $check;
    }

    /**
     * Install dayoff feld
     */
    public function InstallDayOff($data) {
        return $this->model->create($data);
    }

    /**
     * Update dayoff feld
     */
    public function updateDayoff($data, $id) {
        return $this->model->update($data, $id, $attribute="id");
    }

    /**
     * Get data dayoff feld
     */
    public function dayoff($data) {
        return $this->model->create($data);
    }

    public function getlist () {
        return $this->model->where('setting_name', 'dayoff')->first();
    }

    /* check message isset */
    /**
     * Check row missing issert
     */

    public function checkIssertMessage() {

        $check = $this->model->where('setting_name', 'Message')->first();


        if(count($check) == 0 ) {

            return false;

        }

        return $check;
    }

}