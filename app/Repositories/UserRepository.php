<?php namespace TMSApp\Repositories;
 
use TMSApp\Repositories\Contracts\RepositoryInterface;
use TMSApp\Repositories\Eloquent\Repository;

class UserRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'TMSApp\Models\User';
    }

    /**
     * @param string $pattern
     * @param array $columns
     * @return mixed
     */
    public function search($pattern, $columns = array('*')) {

        return $this->model
                    ->where('name', 'LIKE', $pattern['name'])
                    ->where('email', 'LIKE', $pattern['email'])
                    ->get($columns);
    }
    
    /**
    *@param array $list_user
    *@return list user not in project
    */

    public function getListUser($list_user)
    {
        // return $this->model->whereNotIn('id',$list_user)->paginate(TMS_PERPAGE);
        return $this->model->whereNotIn('id',$list_user)->get();
    }

    /**
    *@param $id user
    *@return detail user
    */

    public function getDetailUser ($id)
    {
        return $this->model->find($id);
    }
    
    /**
     * @param 
     * @return listUser has delete
     */
    public function getUserOlyTrash( ) {
        $data = $this->model->select('id')->onlyTrashed()->get()->toArray();
        $listUserDelete = [];

        foreach ($data as $value) {
            $listUserDelete[] = $value['id'];
        }
        return $listUserDelete;
    }

    /**
    *@param 
    *@return list id user
    */

    public function getListID ()
    {
        return $this->model
                    ->select('id')
                    ->get();
    }

    public function getName ($id) {
        $data = $this->model->find($id);
        return $data->name;
    }
}