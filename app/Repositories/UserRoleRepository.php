<?php namespace TMSApp\Repositories;
 
use TMSApp\Repositories\Contracts\RepositoryInterface;
use TMSApp\Repositories\Eloquent\Repository;

class UserRoleRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'TMSApp\Models\UserRole';
    }

    /**
    *
    * @param $array
    * @return mixed
    */
    public function createUserRole($data)
    {

        return $this->model->insert($data);

    }   
     
}