<?php
//degree config
define('TMS_CATEGORY', 1); //for category in category table.
define('TMS_PROCESS', 2); //for process in category table.
define('TMS_ADMIN', 'admin'); //for Role Admin.
define('TMS_PERPAGE', 10); //for the items of page.
define('TMS_STATUS', 'success'); //for response status.
define('TMS_SEARCH_PERPAGE', 20); //for the items of search page.
define('TMS_SEARCH_ALL', 20000000000); //for all items of search page.

define('TMS_MISSING', 0); //for missing report type forget.
define('TMS_DAYOFF', 1); //for missing report type day-off.
define('TMS_WEEKEND', 2); //for day weekend.
define('TMS_REPORT_DONE', 3); //for day report done.

define('TMS_CAN_DAYOFF', 1); //for user missing report can set day off.
define('TMS_NOT_DAYOFF', 0); //for user missing report cann't set day off.


define('TMS_END_DAY', 96); //for time report end day.
define('TMS_TIME', "00:00,00:15,00:30,00:45,01:00,01:15,01:30,01:45,02:00,02:15,02:30,02:45,03:00,03:15,03:30,03:45,04:00,04:15,04:30,04:45,05:00,05:15,05:30,05:45,06:00,06:15,06:30,06:45,07:00,07:15,07:30,07:45,08:00,08:15,08:30,08:45,09:00,09:15,09:30,09:45,10:00,10:15,10:30,10:45,11:00,11:15,11:30,11:45,12:00,12:15,12:30,12:45,13:00,13:15,13:30,13:45,14:00,14:15,14:30,14:45,15:00,15:15,15:30,15:45,16:00,16:15,16:30,16:45,17:00,17:15,17:30,17:45,18:00,18:15,18:30,18:45,19:00,19:15,19:30,19:45,20:00,20:15,20:30,20:45,21:00,21:15,21:30,21:45,22:00,22:15,22:30,22:45,23:00,23:15,23:30,23:45,23:59");// for validate time report restrict.
define('TMS_TIME_RESTRICT', "00:00,00:15,00:30,00:45,01:00,01:15,01:30,01:45,02:00,02:15,02:30,02:45,03:00,03:15,03:30,03:45,04:00,04:15,04:30,04:45,05:00,05:15,05:30,05:45,06:00,06:15,06:30,06:45,07:00,07:15,07:30,07:45,08:00,08:15,08:30,08:45,09:00,09:15,09:30,09:45,10:00,10:15,10:30,10:45,11:00,11:15,11:30,13:00,13:15,13:30,13:45,14:00,14:15,14:30,14:45,15:00,15:15,15:30,15:45,16:00,16:15,16:30,16:45,17:00,17:15,17:30,17:45,18:00,18:15,18:30,18:45,19:00,19:15,19:30,19:45,20:00,20:15,20:30,20:45,21:00,21:15,21:30,21:45,22:00,22:15,22:30,22:45,23:00,23:15,23:30,23:45,23:59");// for validate time report.
define('TMS_EMAIL_SUPPORT', 'anhvutran1608@gmail.com');
define('TMS_EMAIL_TITLE', '[TMS]. password has changed');

define('TMS_OT_TYPE', 1);       //for OT pending type of report - not approve and not reject
define('TMS_NORMAL_TYPE', 0);   // for normal type of report 
define('TMS_WORKOFF_TYPE', 2);   // for workoff type of report  (absence pending)
define('TMS_OT_ACCEPT_TYPE', 3); //for OT accepted type of report
define('TMS_OT_REJECT_TYPE', 4); //for OT rejected type of report
define('TMS_ABSENCE_ACCEPT_TYPE', 5); //for ABSENCE accepted type of report
define('TMS_ABSENCE_REJECT_TYPE', 6); //for ABSENCE rejected type of report

define('TMS_NOTE_DAYOFF', 'Day off'); //for dayoff note of report

define('TMS_MESSAGE_MUST_ADMIN', 'You must be admin'); 
// define('TMS_TICKET_REGEX_VALIDATE', '/^\+?[0-9][\d]*$/'); 
define('TMS_TICKET_REGEX_VALIDATE', '/^[0-9]+$/'); 

// For option parameter to seperate screens
define('TMS_OPTION_OVERTIME_REPORT', 0);        //for OVERTIME normal option
define('TMS_OPTION_OVERTIME_MANAGERMENT', 1);   //for OVERTIME mangagement option
define('TMS_OPTION_ABSENCE_REPORT', 2);         //for ABSENCE normal option
define('TMS_OPTION_ABSENCE_MANAGERMENT', 3);    //for ABSENCE mangagement option

define('TMS_NULL_SYMBOL', '--'); 
define('TMS_TIME_ZONE', 'Asia/Ho_Chi_Minh');// for time zone of system.
define('TMS_FULL_DAY', 8 * 60);
define('TMS_MORNING_SESSION', 3.5 * 60);
define('TMS_AFTERNOON_SESSION', 4.5 * 60);

define('TMS_CATEGORY_NULL', 1);
define('TMS_PROCESS_NULL', 2);

define('TMS_THROWN_DAYOFF_DAYINVALID', 30);
define('TMS_THROWN_DAYOFF_REPORTINDAY', 31);

// Development Category ID
define('DEVELOPMENT_CATEGORY_ID', 40);
define('DEVELOPMENT_PROCESS_ID', 46);




