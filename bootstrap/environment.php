<?php
/*
|--------------------------------------------------------------------------
| Detect The Application Environment
|--------------------------------------------------------------------------
|
| Laravel takes a dead simple approach to your application environments
| so you can just specify a machine name for the host that matches a
| given environment, then we will automatically detect it for you.
|
*/

if (!function_exists('get_env_custom')) {
    
    function get_env_custom($path) {

        if (preg_match("/tms-server/i", $path )) {
            return 'local';
        }
        
        if (preg_match("/tms-dev/i", $path )) {
            return 'develop';
        }
        
        if (preg_match("/tms/i", $path )) {
            return 'production';
        }

        return 'local';
    }
}

$env = $app->detectEnvironment(function(){
    $environmentPath = __DIR__.'/../.env';
    $setEnv = get_env_custom($environmentPath);
    putenv("APP_ENV=$setEnv");
    if (getenv('APP_ENV') && file_exists(__DIR__.'/../.' .getenv('APP_ENV') .'.env')) {
        Dotenv::load(__DIR__ . '/../', '.' . getenv('APP_ENV') . '.env');
    }
});