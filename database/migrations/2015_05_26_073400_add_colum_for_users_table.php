<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumForUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->tinyInteger('status');
            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('id')->on('settings')->onDelete('cascade');
            $table->string('answer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
            // $table->dropColumn('status');
            // $table->dropForeign('users_question_id_foreign');
            // $table->dropColumn('question_id');
            // $table->dropColumn('answer');
        });
    }

}
