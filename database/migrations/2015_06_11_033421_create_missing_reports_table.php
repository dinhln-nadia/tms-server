<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMissingReportsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missing_reports', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('note',255);
            $table->integer('type');
            $table->boolean('day_1');
            $table->boolean('day_2');
            $table->boolean('day_3');
            $table->boolean('day_4');
            $table->boolean('day_5');
            $table->boolean('day_6');
            $table->boolean('day_7');
            $table->boolean('day_8');
            $table->boolean('day_9');
            $table->boolean('day_10');
            $table->boolean('day_11');
            $table->boolean('day_12');
            $table->boolean('day_13');
            $table->boolean('day_14');
            $table->boolean('day_15');
            $table->boolean('day_16');
            $table->boolean('day_17');
            $table->boolean('day_18');
            $table->boolean('day_19');
            $table->boolean('day_20');
            $table->boolean('day_21');
            $table->boolean('day_22');
            $table->boolean('day_23');
            $table->boolean('day_24');
            $table->boolean('day_25');
            $table->boolean('day_26');
            $table->boolean('day_27');
            $table->boolean('day_28');
            $table->boolean('day_29');
            $table->boolean('day_30');
            $table->boolean('day_31');
            $table->tinyInteger('month');
            $table->integer('year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('missing_reports');
    }

}
