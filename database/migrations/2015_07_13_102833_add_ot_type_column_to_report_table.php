<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Output\ConsoleOutput;

class AddOtTypeColumnToReportTable extends Migration {

    private $output;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $output = new ConsoleOutput();

        if (! Schema::hasColumn('reports', 'type'))
        {
            $output->writeln('Creating OT field in reports table...');

            Schema::table('reports', function($table)
            {
                $table->integer('type')->default(TMS_NORMAL_TYPE);
            });
        } else {
            
            $output->writeln('Can\'t create OT field in reports table...');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $output = new ConsoleOutput();

        if (Schema::hasColumn('reports', 'type'))
        {
            Schema::table('reports', function($table)
            {
                $table->dropColumn('type');
                
            });

            $output->writeln('Deleting OT field in reports table...');

        } else {
            $output->writeln('Can\'t delete OT field in reports table...');
        }
    }

}
