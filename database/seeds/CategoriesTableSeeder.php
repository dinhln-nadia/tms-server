<?php
use Illuminate\Database\Seeder;
use TMSApp\Models\Categories;

class CategoriesTableSeeder extends Seeder {

    public function run()
    {
        $categories      = [];
        $now        = Carbon\Carbon::now();

        $category   = ['function development', 'other'];
        $process    = ['Develop', 'other', 'Test_Design'];
        $tmp1       = count($category);
        $tmp2       = count($process);
        Categories::truncate();

        Categories::insert(
            [   'name'          => '-',
                'type'          => TMS_CATEGORY,
                'created_at'    => $now,        
                'updated_at'    => $now]
            );
        Categories::insert(
            [   'name'          => '-',
                'type'          => TMS_PROCESS,
                'created_at'    => $now,        
                'updated_at'    => $now,
            ]);
        
        for ($i=0; $i < $tmp1; $i++) { 
            $categories[] = [
                'name'          => $category[$i],
                'type'          => TMS_CATEGORY,
                'created_at'    => $now,        
                'updated_at'    => $now,
            ];
        }

        for ($i=0; $i < $tmp2; $i++) { 
            $categories[] = [
                'name'          => $process[$i],
                'type'          => TMS_PROCESS,
                'created_at'    => $now,        
                'updated_at'    => $now,
            ];
        }

        Categories::insert($categories);
    }

}