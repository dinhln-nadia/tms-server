<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Laracasts\TestDummy\Factory;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        Model::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call('DatabaseSeederTableSettings');
        $this->call('UserTableSeeder');
        $this->call('CategoriesTableSeeder');
        $this->call('ProjectsTableSeeder');  
        $this->call('ReportsTableSeeder');   
        $this->call('ProjectUserTableSeeder');
        $this->call('ReportsCategoryTableSeeder');
        $this->call('MissingReportsTableSeeder');

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
