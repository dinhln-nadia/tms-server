<?php
use Illuminate\Database\Seeder;
use TMSApp\Models\Settings;
class DatabaseSeederTableSettings extends Seeder {

    public function run()
    {
        Settings::truncate();
        
        $now = Carbon\Carbon::now();
        Settings::insert(array(
                            array('setting_name'=>'question', 'setting_value' => 'Con vật bạn yêu quý nhất là con vật nào ?', 'created_at' => $now, 'updated_at' => $now),
                            array('setting_name'=>'question', 'setting_value' => 'Bạn trường cấp 2 của bạn nằm ở đâu ?', 'created_at' => $now, 'updated_at' => $now),
                            array('setting_name'=>'question', 'setting_value' => 'bạn thích con số mấy ?', 'created_at' => $now, 'updated_at' => $now)
                        ));

    }

}