<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use TMSApp\Models\MissingReports;
use TMSApp\Models\Projects;
use TMSApp\Models\User;
use Carbon\Carbon;
class MissingReportsTableSeeder extends Seeder {
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    function run()
    {
        Model::unguard();

        //clear database
        MissingReports::truncate();

        $missing_report = [];
        $users          = User::select('id')->get();
        $count_users    = count($users);

        for ($j=1;  $j <= $count_users; $j++) { 
            $missing_report = [
                'user_id'   => $j,
                'note'      => 'missing report' . $j+1,
                'type'      => 2,
                'day_1'     => TMS_MISSING,
                'day_2'     => 1,
                'day_3'     => TMS_MISSING,
                'day_4'     => TMS_MISSING,
                'day_5'     => 0,
                'day_6'     => 0,
                'day_7'     => 0,
                'day_8'     => 1,
                'day_9'     => 0,
                'day_10'     => TMS_MISSING,
                'day_11'     => 0,
                'day_12'     => 1,
                'day_13'     => 0,
                'day_14'     => TMS_DAYOFF,
                'day_15'     => 0,
                'day_16'     => 0,
                'day_17'     => 0,
                'day_18'     => TMS_DAYOFF,
                'day_19'     => 0,
                'day_20'     => 1,
                'day_21'     => 0,
                'day_22'     => 0,
                'day_23'     => TMS_DAYOFF,
                'day_24'     => 0,
                'day_25'     => 0,
                'day_26'     => 0,
                'day_27'     => TMS_DAYOFF,
                'day_28'     => 0,
                'day_29'     => 0,
                'day_30'     => 0,
                'day_31'     => 0,
                'month'     => 6,
                'year'     => 2015,
            ];

            try {
                MissingReports::insert($missing_report);
            } catch (Exception $e) {
                echo $user_id."\n";
                echo $e->getMessage();
                echo "=====================\n";
            }
        }
    }
}