<?php
use Illuminate\Database\Seeder;
use TMSApp\Models\ProjectUser;
use TMSApp\Models\Reports;

class ProjectUserTableSeeder extends Seeder {

    public function run()
    {
        $now        = Carbon\Carbon::now();
        $data       = ['admin', 'user1', 'user2', 'user3'];
        $reports    = Reports::select(['project_id', 'user_id'])->get();


        ProjectUser::truncate();
        
        foreach ($reports as $report) {

            $tmp = [
                'project_id'    => $report->project_id,
                'user_id'       => $report->user_id,
                'created_at'    => $now,        
                'updated_at'    => $now,
            ];
            try {
               ProjectUser::insert($tmp);
            } catch (Exception $e) {
                echo "\n";
                echo ''.$report->project_id.'-'.$report->user_id."\n";
                echo $e->getMessage();
                echo "\n";
            }
        }
    }

}