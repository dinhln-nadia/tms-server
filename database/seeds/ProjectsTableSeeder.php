<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use TMSApp\Models\Projects;

class ProjectsTableSeeder extends Seeder {
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    function run()
    {
        Model::unguard();

        //clear database
        Projects::truncate();

        Projects::insert([   'name'          => '-']);

        $now    = Carbon\Carbon::now();
        $name   =['DOCOMO-FUJI-WIFI','KNOCK','AARON','OJS','NODE','TMS'];
        for ($i=0; $i < 6 ; $i++) { 
            $project[] = [
                'name'=> $name[$i],
            ];
        }
        Projects::insert($project);
    }
}