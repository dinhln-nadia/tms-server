<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use TMSApp\Models\ReportCategory;
use TMSApp\Models\Categories;
use TMSApp\Models\Reports;

class ReportsCategoryTableSeeder extends Seeder {
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    function run()
    {
        Model::unguard();

        //clear database
        // ReportCategory::truncate();

        $now    = Carbon\Carbon::now();
        $categories    = Categories::select('id')->where('type', '=', TMS_CATEGORY)->get();
        $process_category    = Categories::select('id')->where('type', '=', TMS_PROCESS)->get();

        $reports= Reports::select('id')->get();
        $tmp        = count($reports);

        //seed category
        for ($i=0; $i < $tmp ; $i++) { 
            $category = $categories->random()->id;
            $report = $reports[$i]->id;

            $result = [
                'category_id'    => $category,
                'report_id'       => $report,
                'created_at'    => $now,
                'updated_at'    => $now,
            ];
            try {
                ReportCategory::insert($result);
            } catch (Exception $e) {
                echo $e->getMessage();
                echo "=====================\n";
            }
        }
        //seed process
        for ($i=0; $i < $tmp ; $i++) { 
            $process = $process_category->random()->id;
            $report = $reports[$i]->id;

            $result = [
                'category_id'    => $process,
                'report_id'       => $report,
                'created_at'    => $now,
                'updated_at'    => $now,
            ];
            try {
                ReportCategory::insert($result);
            } catch (Exception $e) {
                echo $e->getMessage();
                echo "=====================\n";
            }
        }

    }
}