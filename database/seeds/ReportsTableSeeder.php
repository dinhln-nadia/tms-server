<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use TMSApp\Models\Reports;
use TMSApp\Models\Projects;
use TMSApp\Models\User;

class ReportsTableSeeder extends Seeder {
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    function run()
    {
        Model::unguard();

        //clear database
        // Reports::truncate();

        $report = [];
        $now    = Carbon\Carbon::create(2015, 06, 23, 12,45,00);
        $name   =['DOCOMO-FUJI-WIFI','KNOCK','AARON','OJS','NODE','TMS'];

        $project    = Projects::select('id')->get();
        $users      = User::select('id')->get();
        $count_project        = count($project);
        $count_users        = count($users);

        for ($j=0;  $j < $count_users; $j++) {
            $user_id = $users[$j]->id;
            $start_time =Carbon\Carbon::create(2015, 06, 25, 03,15,00);
            $end_time =Carbon\Carbon::create(2015, 06, 25, 03,45,00);
             for ($i=0; $i < $count_project ; $i++) { 
                $start_time = $start_time->subMinutes(30);
                $end_time = $end_time->subMinutes(30);
                $project_id = $project[$i]->id;
                $report = [
                    'project_id'    => $project_id,
                    'user_id'       => $user_id,
                    'note'          => 'note ' . $i,
                    'end_time'      => $end_time,
                    'start_time'    => $start_time,
                    'created_at'    => $now,
                    'ticket'        => '12'.$i,
                    'cost'          => '120',
                    'updated_at'    => $now,
                ];

                try {
                    Reports::insert($report);
                } catch (Exception $e) {
                    echo ''.$project_id.'-'.$user_id."\n";
                    echo $e->getMessage();
                    echo "=====================\n";
                }
            }
        }
    }
}