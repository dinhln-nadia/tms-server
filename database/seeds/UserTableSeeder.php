<?php
use Illuminate\Database\Seeder;
use TMSApp\Models\User;
use TMSApp\Models\Settings;
use Bican\Roles\Models\Role;

class UserTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker\Factory::create('Person');
        $users      = [];
        $password   = bcrypt('12345678');
        $setting    = Settings::select('id')->get();

        User::truncate();
        Role::truncate();
        $users[] = [
                'name'          => 'admin',
                'email'         => 'admin@gmail.com',
                'password'      => $password,
                'created_at'    => $faker->dateTime($max = 'now'),
                'updated_at'    => $faker->dateTime($max = 'now'),
                'status'        => 1
                // 'question_id'   => $setting->random()->id,
                // 'answer'        =>'câu trả lời của admin',
            ];


        User::insert($users);

        $admin = Role::create([
            'name' => 'admin',
            'slug' => 'admin',
            'description' => '' 
        ]);

        $employee = Role::create([
            'name' => 'Employee',
            'slug' => 'employee',
            'description' => '' 
        ]);

        $user = User::find(1)->attachRole($admin); 
    }

}