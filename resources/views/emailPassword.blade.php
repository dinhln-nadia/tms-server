<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>

        <h4>Hello {{ $email }}</h4>

        <div> <p>Your Password has been changed. Please enter this following code to login ( <b> {{ $string }} </b>) </p>
              <p>Best Regards, </p>
              <p>Time management system !</p>
        </div>
    </body>
</html>